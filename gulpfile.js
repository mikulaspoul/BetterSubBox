var gulp = require('gulp'),
    coffee = require('gulp-coffee'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    htmlmin = require('gulp-htmlmin'),
    del = require('del'),
    wiredep = require('wiredep').stream,
    argv = require('yargs').argv,
    gulpIf = require('gulp-if'),
    usemin = require('gulp-usemin'),
    cleanCss = require('gulp-clean-css'),
    runSequence = require('run-sequence'),
    order = require("gulp-order"),
    stylus = require("gulp-stylus"),
    notify = require("gulp-notify"),
    replace = require("gulp-replace"),
    shell = require("gulp-shell"),
    fs = require("fs");

var isBuild = false;

if (argv.build) {
    isBuild = true;
}

var frontendSrc = "frontend_src";
var static_dir = "static";
var build = "build";

var paths = {

    templatesSources: [
        frontendSrc + "/django_templates/**/*.html",
        "!" + frontendSrc + "/django_templates/dynamic_layout.html",
        "!" + frontendSrc + "/django_templates/static_layout.html",
        "!" + frontendSrc + "/django_templates/landing.html"
    ],
    templatesDest: "templates/",
    dynamicLayout: [
        frontendSrc + "/django_templates/dynamic_layout.html",
        frontendSrc + "/django_templates/static_layout.html",
        frontendSrc + "/django_templates/landing.html"
    ],
    dynamicLayoutSources: frontendSrc + "/django_templates/",
    dynamicLayoutDest: build,
    coffeeSources: [
        frontendSrc + "/scripts/{,*/}*.coffee"
    ],
    coffeeOrder: [
        frontendSrc + "/scripts/app.js",
        frontendSrc + "/scripts/filters.js",
        frontendSrc + "/scripts/directives.js",
        frontendSrc + "/scripts/**/*.js"
    ],
    coffeeDest: static_dir + "/scripts/",
    cssSources: [frontendSrc + "/css/*.css"],
    cssDest: static_dir + "/css/",
    fontsSources: [
        frontendSrc + "/bower/font-awesome/fonts/*",
        frontendSrc + "/bower/bootstrap/fonts/*"
    ],
    fontsDest: static_dir + "/fonts/",
    imgSources: [
        frontendSrc + "/img/{,*/}*",
        frontendSrc + "/bower/select2/select2x2.png",
        frontendSrc + "/bower/select2/select2.png",
        frontendSrc + "/bower/select2/select2-spinner.gif"
    ],
    imgDest: static_dir + "/img/",
    ravenSource: [
        frontendSrc + "/bower/raven-js/dist/raven.min.js"
    ],
    jsSources: [
        frontendSrc + "/js/{,*/}*.js"
    ],
    jsDest: static_dir + "/js/",
    audioSources: [
        frontendSrc + "/audio/{,*/}*"
    ],
    audioDest: static_dir + "/audio/",
    viewsSources: [
        frontendSrc + "/views/{,*/}*.html"
    ],
    viewsDest: static_dir + "/views/",
    stylesSources: [
        frontendSrc + "/styles/*.styl"
    ],
    stylesDest: static_dir + "/styles/"
};

gulp.task("clean", function(cb) {
    return del([build, static_dir, paths.templatesDest], cb);
});

gulp.task("copyTemplates", function() {
    var fileContent = fs.readFileSync("version", "utf8");

    return gulp.src(paths.templatesSources)
        .pipe(replace("--version--", fileContent))
        // .pipe(gulpIf(isBuild, htmlmin({collapseWhitespace: true})))
        .pipe(gulp.dest(paths.templatesDest))
});

gulp.task("copyViews", function() {
    return gulp.src(paths.viewsSources)
        .pipe(gulpIf(isBuild, htmlmin({collapseWhitespace: true, sortClassName: true, removeComments: true})))
        .pipe(gulp.dest(paths.viewsDest))
});

gulp.task("copyCss", function() {
    return gulp.src(paths.cssSources)
        .pipe(gulpIf(isBuild, cleanCss()))
        .pipe(gulp.dest(paths.cssDest))
});

gulp.task("copyImg", function() {
    return gulp.src(paths.imgSources)
        .pipe(gulp.dest(paths.imgDest))
});

gulp.task("copyAudio", function() {
    gulp.src(paths.audioSources)
        .pipe(gulp.dest(paths.audioDest))
});

gulp.task("copyFonts", function() {
    return gulp.src(paths.fontsSources)
        .pipe(gulp.dest(paths.fontsDest))
});

gulp.task("scripts", function() {
    gulp.src(paths.coffeeSources)
        .pipe(coffee())
        .on("error", notify.onError({
          message: "Error: <%= error %>",
          title: "Error compiling scripts"
        }))
        .pipe(order(paths.coffeeOrder))
        .pipe(gulpIf(isBuild, uglify({mangle: false})))
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest(paths.coffeeDest));
});

gulp.task('watch', function() {
  gulp.watch(paths.coffeeSources, ['scripts']);
  gulp.watch(paths.viewsSources, ['copyViews']);
  gulp.watch(paths.templatesSources, ['copyTemplates']);
  gulp.watch(paths.dynamicLayout, ['afterUsemin']);
  gulp.watch(paths.cssSources, ['copyCss']);
  gulp.watch(paths.imgSources, ['copyImg']);
  gulp.watch(paths.audioSources, ['copyAudio']);
  gulp.watch(paths.jsSources, ['afterUsemin']);
  gulp.watch(paths.stylesSources, ['stylus']);
});

gulp.task('bower', function () {
  return gulp.src(paths.dynamicLayout)
    .pipe(wiredep())
    .pipe(gulp.dest(paths.dynamicLayoutSources));
});

gulp.task('usemin', function() {
  return gulp.src(paths.dynamicLayout)
    .pipe(usemin({
      css: [ gulpIf(isBuild, cleanCss()),
             replace("'select2.png'", "'/static/img/select2.png'"),
             replace("'select2x2.png'", "'/static/img/select2x2.png'"),
             replace("'select2-spinner.gig'", "'/static/img/select2-spinner.gif'")
           ],
      nondynamicCss: [ gulpIf(isBuild, cleanCss()) ],
      nondynamicJs: [ gulpIf(isBuild, uglify({mangle: false})) ],
      landingCss: [ gulpIf(isBuild, cleanCss()) ],
      landingJs: [ gulpIf(isBuild, uglify({mangle: false})) ],
      js: [ gulpIf(isBuild, uglify({mangle: false})) ]
    }))
    .pipe(gulp.dest(paths.dynamicLayoutDest));
});

gulp.task('afterUsemin', ['usemin'], function() {
  gulp.src(["build/*.html"])
    .pipe(gulp.dest(paths.templatesDest));
  gulp.src(["build/static/scripts/*.js"])
    .pipe(gulp.dest(paths.coffeeDest));
  gulp.src(["build/static/styles/*.css"])
    .pipe(gulp.dest(static_dir+"/styles/"));
});


gulp.task('stylus', function() {
  gulp.src(paths.stylesSources)
    .pipe(gulpIf(isBuild, stylus({
        compress: true
      })))
    .pipe(gulpIf(!isBuild, stylus()))
    .on("error", notify.onError({
          message: "Error: <%= error %>",
          title: "Error compiling stylus"
        }))
    .pipe(gulp.dest(paths.stylesDest))
});

gulp.task('copyRaven', function () {
    gulp.src(paths.ravenSource).pipe(gulp.dest(paths.jsDest));
});

gulp.task('getVersion', shell.task("scripts/get_version.sh"));

// The default task (called when you run `gulp` from cli)
gulp.task('default', function () {
    var second_sequence = ['getVersion', 'afterUsemin', 'scripts', 'copyViews', 'copyTemplates', 'copyCss',
                       'copyImg', 'copyAudio', 'copyFonts', 'stylus', 'copyRaven'];
    if (!isBuild) {
      second_sequence.push("watch")
    }

    return runSequence(['clean', 'bower'], second_sequence)
});
