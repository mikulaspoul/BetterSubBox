## BetterSubBox

For details about the project, see https://bettersubbox.com/. Pull requests and issues welcomed. Released under MIT.

# Used technologies

 * Django
 * Angular 1
 * Redis
 * Celery

# Requirements to run

 * Python 3.6
 * [Pipenv](https://docs.pipenv.org/)
 * PostgreSQL database
 * Redis
 * YouTube API credentials
 * Node (for build only)
 * Sentry (bug reporting)

# How to run locally

1. Clone the repository
2. `pipenv sync`
3. Create a `local_settings.py` file with all needed settings. (See `local_settings.example.py` file for an example, running `manage.py` will throw `ImproperlyConfigured` if anything is missing.
4. `python manage.py migrate`
5. `npm install; bower install`
6. Now run these  three commands simultaneously:
    * `gulp`
    * `celery worker --app=apps.celery_config:app --loglevel=INFO -c 1`
    * `python manage.py runserver`
7. The website now should be accesible at `127.0.0.1:8000`

# Deploying

Production files for Apache and Supervisor are included in the in `wsgi` and `etc/supervisor` folders,
that should be examples enough. The applications needs a cron as  well, but due the usage of the library `django_cron`
only one entry is required.

```
* * * * * /<path_to_python>/python /<path_to_project>/manage.py runcrons
```
