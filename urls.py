# encoding=utf-8
from __future__ import unicode_literals, print_function

from django.conf.urls import include, url, static
from django.conf import settings
from django.urls import reverse_lazy, path
from django.views.generic import RedirectView, TemplateView
from django.contrib import admin


from apps.core.views import LoginView, IndexView, LogoutView, StatisticsView, StaticPage, error500, error404
from apps.video.views import FullPageView, RandomVideoView
from apps.news.views import ReadNews
from apps.custom_user.views import DataReadView, DisconnectCallbackView, EnableWriteAccessCompleteView
from apps.youtube.views import (SynchronizeView, LoadingDataView, YoutubeDownView, SynchronizationSyncResultView,
                                BulkSubscriptionDeleteView, SynchronizeNewRequest, LoadWatchedTaskResult,
                                UnsubscribeView)

from apps.core.api import CustomApi
from apps.custom_user.api import CustomUserResource
from apps.channel.api import ChannelResource, SubscriptionResource, AutoIgnoreResource
from apps.video.api import VideoResource, StateResource
from apps.group.api import GroupResource, GroupMembershipResource

admin.autodiscover()


urlpatterns = [
    path('admin/', admin.site.urls),

    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),

    url(r'^unsubscribe/(?P<pk>[PK0-9]+)/$', UnsubscribeView.as_view(), name="unsubscribe"),
    url(r'^synchronize/$', SynchronizeView.as_view(), name="synchronize"),
    url(r'^synchronize/bulk_delete/$', BulkSubscriptionDeleteView.as_view(), name="synchronize_bulk_delete"),
    url(r'^synchronize/result/$', SynchronizationSyncResultView.as_view(), name="synchronize_result"),
    url(r'^synchronize/new_request/$', SynchronizeNewRequest.as_view(), name="synchronize_new_request"),

    url(r'^load_watched/result/$', LoadWatchedTaskResult.as_view(), name="load_watched_result"),

    url(r'^enable_write_access/$', RedirectView.as_view(
        url=reverse_lazy('social:begin', args=['google-oauth2']),
        query_string=True
    ), name="enable_write_access"),

    url(r'^enable_write_access/complete/$', EnableWriteAccessCompleteView.as_view(), name="enable_write_access"),

    url(r'^statistics/$', StatisticsView.as_view(), name='statistics'),
    url(r'^loading_data/$', LoadingDataView.as_view(), name='loading_data'),
    url(r'^youtube_down/$', YoutubeDownView.as_view(), name='youtube_down'),
    url(r'^account_switched/$', TemplateView.as_view(template_name="account_switched.html"), name='account_switched'),

    url(r'^user/data_ready/$', DataReadView.as_view(), name="data_ready"),
    url(r'^user/disconnect_callback/$', DisconnectCallbackView.as_view(), name="disconnect_callback"),
    url(r'^disconnected/$', TemplateView.as_view(template_name="disconnected.html"), name="disconnected"),
    url(r'^watch/(?P<video>[a-zA-Z0-9-_]{11,})/$', FullPageView.as_view(), name='full_page'),
    url(r'^video/random/$', RandomVideoView.as_view(), name="random_video"),
    url(r'^news/read/$', ReadNews.as_view(), name='read_news'),
    url(r'^favicon.ico$', RedirectView.as_view(url='static/img/favicon.ico', permanent=False)),

    url(r'^changelog/$', StaticPage.as_view(template_name="changelog.html"), name="changelog"),
    url(r'^robots.txt', TemplateView.as_view(template_name="robots.txt", content_type="text/plain")),
]

# ouath2 urls
urlpatterns += [
    url('', include('social_django.urls', namespace='social')),
]

# static files on production are served from server directly, not through python
if settings.DEBUG:
    urlpatterns += static.static(
        settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += [
        url(r'^500/', error500, name="sample_500"),
        url(r'^404/', error404, name="sample_404"),
    ]
else:
    handler404 = 'core.views.error404'
    handler500 = 'core.views.error500'


v1_api = CustomApi(api_name="v1")
v1_api.register(CustomUserResource())
v1_api.register(ChannelResource())
v1_api.register(AutoIgnoreResource())
v1_api.register(SubscriptionResource())
v1_api.register(VideoResource())
v1_api.register(StateResource())
v1_api.register(GroupResource())
v1_api.register(GroupMembershipResource())

urlpatterns += [
    url(r'^api/', include(v1_api.urls))
]
