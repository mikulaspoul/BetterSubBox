# encoding=utf-8
from datetime import datetime
from typing import List

from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.utils import timezone
from django_cron import CronJobBase, Schedule

from apps.channel.models import Channel, Subscription
from apps.video.models import State
from apps.youtube.tasks import TheColbertTask


class UpdateChannelsCountsAndPrioritiesJob(CronJobBase):
    RUN_AT_TIMES = ['3:20']

    schedule = Schedule(run_at_times=RUN_AT_TIMES)
    code = 'channel.update_counts_and_priorities'

    def analyze_time_of_upload(self, lst: List[datetime]) -> bool:
        """
        :param lst: List of datetimes of published videos
        :return: If there was a date in the list that was really close to other ones
        """
        lst = sorted(lst)

        for i in range(1, len(lst) - 2):

            if (lst[i] - lst[i - 1]).total_seconds() < 5 or (lst[i + 1] - lst[i]).total_seconds() < 5:
                return True

        return False

    def do(self):
        month_ago = timezone.now() - relativedelta(days=30)
        day_ago = timezone.now() - relativedelta(days=1)

        for channel in Channel.objects.all():
            videos_this_month = channel.videos.filter(published__gte=month_ago).count()
            videos_per_day = channel.videos_this_month / 30.0
            upload_times_similar = False

            if videos_per_day > 1 and self.analyze_time_of_upload(
                channel.videos.filter(
                    published__isnull=False
                ).order_by("-published").values_list("published", flat=True)[:20]
            ):
                upload_times_similar = True

            Channel.objects.filter(pk=channel.id).update(
                videos_this_month=videos_this_month,
                videos_per_day=videos_per_day,
                upload_times_similar=upload_times_similar,
            )

            if channel.videos.filter(published__gte=day_ago).count():
                TheColbertTask().delay(channel.id, days=2)

        Channel.objects.filter(videos_per_day=0, videos__isnull=True).update(priority=1)
        Channel.objects.filter(videos_per_day=0, videos__isnull=False).update(priority=2)

        base_queryset = Channel.objects.exclude(subscriptions__deleted=False,
                                                subscriptions__favorite=True)

        channel_count = base_queryset.count()
        priority_size = channel_count / (settings.NUMBER_OF_PRIORITIES - 2)

        Channel.objects.filter(subscriptions__deleted=False,
                               subscriptions__favorite=True).update(priority=settings.NUMBER_OF_PRIORITIES)

        for priority in range(3, settings.NUMBER_OF_PRIORITIES):
            if priority != settings.NUMBER_OF_PRIORITIES:
                queryset = base_queryset.order_by("videos_per_day")[
                    priority * priority_size: (priority + 1) * priority_size + 1
                ]
            else:
                queryset = base_queryset.order_by("videos_per_day")[
                    priority * priority_size:
                ]

            Channel.objects.filter(pk__in=queryset).update(priority=priority + 1)

        for subscription in Subscription.objects.values("id", "user_id", "channel_id"):
            Subscription.objects.filter(pk=subscription["id"]).update(
                watched_this_month=State.objects.filter(
                    user_id=subscription["user_id"],
                    video__channel_id=subscription["channel_id"],
                    video__published__gte=month_ago,
                    watched=True
                ).count()
            )
