# encoding=utf-8
import json

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Case, F, When, FloatField, OuterRef, Subquery
from tastypie import fields
from tastypie.authentication import SessionAuthentication
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from tastypie.exceptions import Unauthorized
from tastypie.http import HttpBadRequest
from tastypie.validation import Validation

from apps.channel.models import Channel, Subscription, AutoIgnore
from apps.core.api import CustomForeignKey, CustomModelResource, ReadOnlyAuthorization
from apps.video.models import Video
from apps.youtube.exceptions import InvalidCredentials, InsufficientPermission


class ChannelResource(CustomModelResource):
    """ Channel API. """

    def get_object_list(self, request):
        return super(ChannelResource, self).get_object_list(request).filter(subscriptions__user_id=request.user.id,
                                                                            subscriptions__deleted=False)

    class Meta:
        queryset = Channel.objects.all()
        resource_name = "channel"
        authentication = SessionAuthentication()
        authorization = ReadOnlyAuthorization()
        always_return_data = True  # even when the list is empty
        list_allowed_methods = ['get']  # readonly
        detail_allowed_methods = ['get']  # readonly
        excludes = ['initial_data_loaded', 'priority', 'videos_per_day']
        filtering = {
            'title': ALL,  # allows searching via title
        }
        ordering = ['title']  # allows ordering

    def dehydrate(self, bundle):
        bundle = super().dehydrate(bundle)

        if bundle.request.GET.get("include_subscription"):
            bundle.data["subscription"] = Subscription.objects.get(user=bundle.request.user,
                                                                   channel=bundle.obj,
                                                                   deleted=False).to_channel_api()

        return bundle

    def apply_filters(self, request, applicable_filters):
        """ Applies custom filters
        :param request: WSGI request
        :param applicable_filters: Possible filters
        :return: filtered objects
        """
        obj_list = super().apply_filters(request, applicable_filters)
        not_in_group = request.GET.get('not_in_group', None)

        if not_in_group:  # custom filter that excludes member of a group.
            obj_list = obj_list.exclude(groups__group__id=not_in_group)

        return obj_list


class SubscriptionAuthorization(ReadOnlyAuthorization):
    """ Authorization that makes sure user can only ready his subscriptions. """
    def read_list(self, object_list, bundle):
        return object_list

    def read_detail(self, object_list, bundle):
        return True

    def create_detail(self, object_list, bundle):
        raise Unauthorized("You can't do that.")

    def update_list(self, object_list, bundle):
        return object_list

    def update_detail(self, object_list, bundle):
        return True

    def delete_list(self, object_list, bundle):
        return object_list

    def delete_detail(self, object_list, bundle):
        return True


class SubscriptionResource(CustomModelResource):
    """ Channel API. """
    user = CustomForeignKey("apps.custom_user.api.CustomUserResource", "user", full=False, pk_only=True)
    channel = CustomForeignKey(ChannelResource, "channel", full=False, pk_only=True)
    watched_this_month_percentage = fields.FloatField("watched_this_month_percentage", null=True, blank=True)

    last_video_published = fields.DateTimeField(
        "last_video_published", null=True, use_in=lambda bundle: bundle.request.GET.get("include_last_video_published")
    )

    def get_object_list(self, request):
        queryset = super().get_object_list(request).filter(user_id=request.user.id)

        if "make_channel_full" in request.GET:
            queryset = queryset.select_related("channel")

            if "calculate_percentage" in request.GET:
                queryset = queryset.annotate(
                    watched_this_month_percentage=Case(
                        When(channel__videos_this_month=0, then=0),
                        default=F("watched_this_month") * 100.0 / F("channel__videos_this_month"),
                        output_field=FloatField()
                    )
                )

            if request.GET.get("include_last_video_published"):
                queryset = queryset.annotate(
                    last_video_published=Subquery(
                        Video.objects.filter(channel_id=OuterRef("channel_id")).values("published")[:1]
                    )
                )

        return queryset

    class Meta:
        queryset = Subscription.objects.all()
        resource_name = "subscription"
        authentication = SessionAuthentication()
        authorization = SubscriptionAuthorization()
        always_return_data = True  # even when the list is empty
        list_allowed_methods = ['get', 'put', 'delete']  # readonly
        detail_allowed_methods = ['get', 'put', 'delete']  # readonly
        excludes = ['initial', 'youtube_id', 'deleted']
        ordering = ["channel", "last_video_published"]
        filtering = {
            'channel': ALL_WITH_RELATIONS,  # allows searching via title,
            'pk': ALL,
        }

    def delete_detail(self, request, **kwargs):
        try:
            return super().delete_detail(request, **kwargs)
        except InvalidCredentials:
            return HttpBadRequest(json.dumps({
                "failed": True,
                "reason": "invalid_credentials"
            }), content_type="application/json")
        except InsufficientPermission:
            return HttpBadRequest(json.dumps({
                "failed": True,
                "reason": "insufficient_permission"
            }), content_type="application/json")

    def apply_sorting(self, obj_list, options=None):
        if options is None:
            options = {}

        if "order_by" not in options:
            return super().apply_sorting(obj_list, options=options)

        order = None

        for i in "watched", "watched_percentage":
            if i == options["order_by"]:
                order = i
                break
            elif "-" + i == options["order_by"]:
                order = "-" + i
                break

        if order is None:
            return super().apply_sorting(obj_list, options=options)
        else:
            return obj_list.order_by(*{
                "watched": ["watched_this_month", "-channel__videos_this_month"],
                "-watched": ["-watched_this_month"],
                "watched_percentage": ["watched_this_month_percentage", "-channel__videos_this_month"],
                "-watched_percentage": ["-watched_this_month_percentage", "-channel__videos_this_month"]
            }[order])

    def apply_filters(self, request, applicable_filters):
        obj_list = super().apply_filters(request, applicable_filters)

        # channel__title__icontains should work fine, but doesn't, workaround
        icontains = request.GET.get("title_icontains")
        if icontains:
            obj_list = obj_list.filter(channel__title__icontains=icontains)

        return obj_list


class AutoIgnoreAuthorization(ReadOnlyAuthorization):
    """ Authorization that makes sure user can only ready his subscriptions. """
    def read_list(self, object_list, bundle):
        return object_list

    def read_detail(self, object_list, bundle):
        return True

    def create_detail(self, object_list, bundle):
        return True

    def delete_list(self, object_list, bundle):
        return object_list

    def delete_detail(self, object_list, bundle):
        return True


class AutoIgnoreValidation(Validation):
    """ Validates users input. """
    def is_valid(self, bundle, request=None):
        errors = {}

        if 'string' not in bundle.data:
            errors['string'] = ["You have set the string."]

        if "subscription" not in bundle.data or not bundle.data["subscription"]:
            errors['subscription'] = ["You have set the subscription."]

        return errors


class AutoIgnoreResource(CustomModelResource):
    """ AutoIgnore API. """
    subscription = CustomForeignKey(SubscriptionResource, "subscription", full=False, pk_only=True)
    user = CustomForeignKey("apps.custom_user.api.CustomUserResource", "user", full=False, readonly=True, null=True)
    channel = CustomForeignKey(ChannelResource, "channel", full=True, readonly=True, null=True)

    def get_object_list(self, request):
        return super().get_object_list(request).filter(
            subscription__user_id=request.user.id,
            subscription__deleted=False
        )

    class Meta:
        queryset = AutoIgnore.objects.select_related("subscription__channel").all()
        resource_name = "auto_ignore"
        authentication = SessionAuthentication()
        authorization = AutoIgnoreAuthorization()
        validation = AutoIgnoreValidation()
        always_return_data = True  # even when the list is empty
        list_allowed_methods = ['get', 'post', 'delete']
        detail_allowed_methods = ['get', 'post', 'delete']

    def hydrate(self, bundle):
        bundle = super().hydrate(bundle)

        if "channel" in bundle.data:
            try:
                bundle.data["subscription"] = Subscription.objects.get(user=bundle.request.user,
                                                                       channel__id=bundle.data["channel"],
                                                                       deleted=False).pk
            except ObjectDoesNotExist:
                pass

        return bundle
