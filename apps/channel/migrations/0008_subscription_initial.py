# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('channel', '0007_auto_20141229_1414'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscription',
            name='initial',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
