# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('channel', '0005_auto_20141123_1244'),
    ]

    operations = [
        migrations.AlterField(
            model_name='channel',
            name='thumbnail',
            field=models.CharField(default=b'', max_length=255, verbose_name='Thumbnail'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='channel',
            name='title',
            field=models.CharField(max_length=255, verbose_name='Title'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='channel',
            name='youtube_id',
            field=models.CharField(unique=True, max_length=255, verbose_name='Youtube id'),
            preserve_default=True,
        ),
    ]
