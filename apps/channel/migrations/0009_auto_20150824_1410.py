# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('channel', '0008_subscription_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscription',
            name='favorite',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='subscription',
            name='youtube_id',
            field=models.CharField(default=b'', max_length=100, verbose_name='Youtube id', blank=True),
            preserve_default=True,
        ),
    ]
