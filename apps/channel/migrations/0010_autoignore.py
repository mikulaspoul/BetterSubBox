# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('channel', '0009_auto_20150824_1410'),
    ]

    operations = [
        migrations.CreateModel(
            name='AutoIgnore',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(verbose_name='Created', auto_created=True)),
                ('string', models.CharField(max_length=100, verbose_name='String')),
                ('subscription', models.ForeignKey(related_name='autoignores', verbose_name='Subscription', to='channel.Subscription',
                                                   on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Auto ignore',
                'verbose_name_plural': 'Auto ignores',
            },
            bases=(models.Model,),
        ),
    ]
