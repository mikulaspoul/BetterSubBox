# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models

from django.utils.text import slugify

def set_slugs(apps, schema_editor):
    Channel = apps.get_model("channel", "Channel")

    for channel in Channel.objects.all():
        channel.slug = slugify("%s %s" % (channel.id, channel.title))
        channel.save()

class Migration(migrations.Migration):

    dependencies = [
        ('channel', '0012_channel_slug'),
    ]

    operations = [
        migrations.RunPython(code=set_slugs)
    ]
