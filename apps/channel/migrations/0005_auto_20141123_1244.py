# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('channel', '0004_auto_20141123_1243'),
    ]

    operations = [
        migrations.AlterField(
            model_name='channel',
            name='thumbnail',
            field=models.CharField(default=b'', max_length=100, verbose_name='Thumbnail'),
            preserve_default=True,
        ),
    ]
