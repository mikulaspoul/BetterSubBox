# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('channel', '0002_channel_initial_data_loaded'),
    ]

    operations = [
        migrations.AlterField(
            model_name='channel',
            name='youtube_id',
            field=models.CharField(unique=True, max_length=100, verbose_name='Youtube id'),
            preserve_default=True,
        ),
    ]
