# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('channel', '0011_auto_20150829_1838'),
    ]

    operations = [
        migrations.AddField(
            model_name='channel',
            name='slug',
            field=models.SlugField(max_length=255, verbose_name='Slug', blank=True),
        ),
        migrations.AddField(
            model_name='subscription',
            name='deleted',
            field=models.BooleanField(default=False),
        ),
    ]
