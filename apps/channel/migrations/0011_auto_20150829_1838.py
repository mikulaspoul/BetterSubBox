# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('channel', '0010_autoignore'),
    ]

    operations = [
        migrations.AlterField(
            model_name='autoignore',
            name='created',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created'),
            preserve_default=True,
        ),
    ]
