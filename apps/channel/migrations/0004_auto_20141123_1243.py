# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('channel', '0003_auto_20141122_2031'),
    ]

    operations = [
        migrations.AddField(
            model_name='channel',
            name='thumbnail',
            field=models.CharField(default=None, max_length=100, verbose_name='Thumbnail', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='subscription',
            name='channel',
            field=models.ForeignKey(related_name='subscriptions', verbose_name='Channel', to='channel.Channel', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
