# encoding=utf-8

from apps import logger, client
from apps.channel.models import AutoIgnore
from apps.video.models import Video, State


def ignore_videos():
    counter = 0
    for ignore in AutoIgnore.objects.select_related("subscription").all():
        for video in Video.objects.filter(channel_id=ignore.subscription.channel_id,
                                          title__icontains=ignore.string.lower(),
                                          published__gte=ignore.created
                                          ).exclude(states__user_id=ignore.subscription.user_id):
            try:
                obj, created = State.objects.get_or_create(video_id=video.id, user_id=ignore.subscription.user_id,
                                                           defaults={"ignored": True})
                if created:
                    counter += 1
            except BaseException:
                client.captureException(extra={
                    "video_id": video.id,
                    "user_id": ignore.subscription.user_id
                })

    if counter:
        logger.info("Auto ignored %s videos" % counter)
