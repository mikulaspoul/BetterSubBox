# encoding=utf-8
from django.db import models
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
from django.conf import settings

from apps.custom_user.models import CustomUser


class Channel(models.Model):
    """
    Model of a youtube channel.
    """

    youtube_id = models.CharField(max_length=255, verbose_name=_("Youtube id"), unique=True)
    title = models.CharField(max_length=255, verbose_name=_("Title"))
    slug = models.SlugField(max_length=255, verbose_name=_("Slug"), blank=True, unique=True)
    description = models.TextField(verbose_name=_("Description"), blank=True)
    initial_data_loaded = models.BooleanField(default=False)
    thumbnail = models.CharField(max_length=255, verbose_name=_("Thumbnail"), blank=False, default="")

    videos_this_month = models.PositiveIntegerField(default=0,
                                                    verbose_name=_("Number of videos uploaded in this month"))
    videos_per_day = models.FloatField(default=0)
    priority = models.PositiveSmallIntegerField(default=1)
    upload_times_similar = models.BooleanField(default=False)

    def save(self, **kwargs):
        if self.id:
            self.make_slug()
            must_after_first_save = False
        else:
            must_after_first_save = True

        super(self.__class__, self).save(**kwargs)

        if must_after_first_save:
            self.make_slug()
            super(self.__class__, self).save(**kwargs)

    def make_slug(self):
        self.slug = slugify("%s %s" % (self.id, self.title))

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Channel")
        verbose_name_plural = _("Channels")

    def no_videos_since_last_watched(self, request):
        from apps.video.models import State

        if request.GET.get("include_watched", "false") == "true":
            return -2  # this metric doesn't make sense for this filter

        user = request.user

        try:
            last_watched_state = State.objects.filter(
                user=user, video__channel=self, watched=True
            ).select_related("video").order_by("-video__published")[0]
        except IndexError:
            return -1  # user didn't watch anything at all

        return self.videos.filter(
            published__gt=last_watched_state.video.published
        ).exclude(
            pk__in=State.objects.filter(user=user).filter(Q(ignored=True) | Q(for_later=True)).values("video")
        ).count()


class SubscriptionManager(models.Manager):

    use_for_related_fields = True

    def get_queryset(self):
        return super(SubscriptionManager, self).get_queryset().filter(deleted=False)


class Subscription(models.Model):
    """
    Model of a users subscription to a channel.
    """

    user: CustomUser = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("User"),
                                         related_name="subscriptions", on_delete=models.CASCADE)
    channel: Channel = models.ForeignKey(Channel, verbose_name=_("Channel"), related_name="subscriptions",
                                         on_delete=models.CASCADE)
    youtube_id = models.CharField(max_length=100, verbose_name=_("Youtube id"), default="", blank=True)
    initial = models.BooleanField(default=False)
    favorite = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)

    watched_this_month = models.PositiveIntegerField(verbose_name=_("Number of videos published this month watched"),
                                                     default=0)

    objects = SubscriptionManager()
    all_objects = models.Manager()

    class Meta:
        verbose_name = _("Subscription")
        verbose_name_plural = _("Subscriptions")
        unique_together = ("user", "channel")  # no duplicate subscriptions

    def delete(self, send_to_youtube=True, using=None):
        if send_to_youtube:
            from apps.youtube.utils import delete_subscription  # recursive import
            delete_subscription(self.user, self.youtube_id)

        Subscription.objects.filter(pk=self.pk).update(deleted=True)

    def to_channel_api(self):
        return {
            'id': self.pk,
            'user': self.user.pk,
            'channel': self.channel.pk,
            'favorite': self.favorite,
        }

    def __str__(self):
        return "Subscription({}, {})".format(self.channel.title, self.user.get_name_or_username())


class AutoIgnore(models.Model):
    """
    User can specify that some strings for certain channels trigger auto-ignore.
    """
    subscription = models.ForeignKey(Subscription, verbose_name=_("Subscription"), related_name="autoignores",
                                     on_delete=models.CASCADE)
    string = models.CharField(verbose_name=_("String"), max_length=100)
    created = models.DateTimeField(verbose_name=_("Created"), auto_now_add=True)

    def get_user(self) -> CustomUser:
        return self.subscription.user

    user: CustomUser = property(get_user)

    def get_channel(self) -> Channel:
        return self.subscription.channel

    channel: CustomUser = property(get_channel)

    class Meta:
        verbose_name = _("Auto ignore")
        verbose_name_plural = _("Auto ignores")
