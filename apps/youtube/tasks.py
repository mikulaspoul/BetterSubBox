# encoding=utf-8
import requests
import logging

from collections import namedtuple
from celery.task import Task
from celery.utils.log import get_task_logger
from dateutil.relativedelta import relativedelta

from django.conf import settings
from django.db.utils import IntegrityError, DataError
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.utils import timezone
from typing import Optional, Dict, Any, Tuple, Union

from apps.channel.models import Channel, Subscription
from apps.custom_user.models import CustomUser
from apps.video.models import Video, State
from apps.youtube.models import SyncLog
from apps.youtube.utils import parse_published
from apps.youtube.exceptions import YouTubeUnavailable
from apps import client

TreatedResult = namedtuple("TreatedTuple", ["counter", "end"])
SubscriptionSyncResult = namedtuple("SubscriptionSyncResult", ["new", "delete"])
logger = get_task_logger(__name__)


class LoadDataTask(Task):
    """ Loads basic videos for new channels.
    """

    @staticmethod
    def get_initial_channel_videos(channel):
        """ Loads the initial 50 videos of the channel.
        :param channel: Channel instance
        :return: number of loaded videos
        """
        counter = 0

        # reload channel and make sure the data are supposed to be loaded
        # (it can happen that the script will run some time and another instance of the scripted is launched from cron)
        channel = Channel.objects.get(pk=channel.pk)
        if channel.initial_data_loaded:
            return counter

        # loads the first fifty videos
        response = requests.get(
            'https://www.googleapis.com/youtube/v3/search',
            params={
                'key': settings.YOUTUBE_API_KEY,
                'channelId': channel.youtube_id,
                'part': 'id,snippet',
                'maxResults': 20,
                'order': 'date',
                'type': 'video'
            }
        )
        data = response.json()

        # reload channel again and make sure the data are supposed to be loaded
        # (it can happen that the script will run some time and another instance of the scripted is launched from cron)
        channel = Channel.objects.get(pk=channel.pk)
        if channel.initial_data_loaded:
            return counter

        if 'items' not in data:
            return counter

        sync_log = SyncLog.objects.create(channel=channel)

        videos = data['items']

        for video in videos:
            # only save the id in the database
            if video['id']['kind'] == 'youtube#video':
                try:
                    new_vid = Video(youtube_id=video['id']['videoId'],
                                    channel=channel,
                                    published=parse_published(video['snippet']['publishedAt']),
                                    title=video['snippet']['title'])
                    new_vid.save()
                    sync_log.add_video(new_vid)
                    counter += 1
                except (IntegrityError, KeyError):
                    pass
                    # only basic details are saved, other details are loaded on request
                except DataError:
                    client.captureException(extra={"video": video})

        return counter

    def run(self):
        for channel in Channel.objects.filter(initial_data_loaded=False):
            try:
                count = self.get_initial_channel_videos(channel)

                logger.info("%s initial videos for channel %s loaded" % (count, channel.title))
            except BaseException:
                client.captureException()
            finally:
                channel.initial_data_loaded = True
                channel.save()


class GetOrRefreshSubscriptionsTask(Task):

    def run(self, user_id):
        """
        Functions that connects to youtube and checks which channel the user is subscribed to.
        """
        user = CustomUser.objects.get(id=user_id)

        # is this the first sync? if so, mark the subscriptions as initial
        if Subscription.objects.filter(user=user).count() == 0:
            initial = True
        else:
            initial = False

        social = user.social_auth.get(provider='google-oauth2')  # users access token

        try:
            response = requests.get(
                'https://www.googleapis.com/youtube/v3/subscriptions',
                params={'access_token': social.extra_data['access_token'],
                        'part': 'id,snippet',
                        'mine': 'true',
                        'maxResults': 50}  # max usable
            )
        except BaseException:
            client.captureException()
            raise YouTubeUnavailable

        data = response.json()

        # access token has run out, reload token and reload response
        if 'error' in data and data['error']['code'] == 401:
            client.captureMessage(
                "youtube.tasks.GetOrRefreshSubscriptionsTask.run failed, error",
                extra={
                    "response": data,
                },
                level=logging.WARNING,
                tags={"youtube": "refresh_subscription"})
            return {"new": [], "delete": [], "failed": True, "reason": 401}

        subscriptions = data['items']  # data from first page

        # load the next pages and append to all data
        while 'nextPageToken' in data:
            response = requests.get(
                'https://www.googleapis.com/youtube/v3/subscriptions',
                params={'access_token': social.extra_data['access_token'],
                        'part': 'snippet',
                        'mine': 'true',
                        'maxResults': 50,
                        'pageToken': data['nextPageToken']}
            )
            data = response.json()
            subscriptions += data['items']

        # printer.pprint(subscriptions)

        subscribed_channels = []
        subscription_ids = {}
        new_channels = 0

        # create new channels or get the existing ones
        for subscription in subscriptions:
            # print subscription['snippet']['channelId']
            try:
                subscribed_channels.append(
                    Channel.objects.get(
                        youtube_id=subscription['snippet']['resourceId']['channelId']
                    )
                )
            except ObjectDoesNotExist:
                new_channel = Channel(youtube_id=subscription['snippet']['resourceId']['channelId'],
                                      title=subscription['snippet']['title'],
                                      description=subscription['snippet']['description'],
                                      thumbnail=subscription['snippet']['thumbnails']['default']['url'])
                new_channel.save()
                new_channels += 1

                # initial videos are loaded via asynchronious

                subscribed_channels.append(new_channel)
            subscription_ids[subscription['snippet']['resourceId']['channelId']] = subscription["id"]

        # print subscribed_channels
        delete = set()
        new = set()

        # delete the subscriptions that are no longer actual
        for existing in user.subscriptions.select_related("channel").all():
            # if there is a record about a subscription that is no longer
            # in the list that was just downloaded, delete the record
            if existing.channel not in subscribed_channels:
                delete.add(existing.id)
                # existing.delete(send_to_youtube=False)

            # otherwise remove the channel from the list,
            # the rest will be used to create subscriptions
            else:
                if existing.youtube_id != subscription_ids[existing.channel.youtube_id]:
                    existing.youtube_id = subscription_ids[existing.channel.youtube_id]
                    existing.save()
                subscribed_channels.remove(existing.channel)

        # create record of the rest
        for channel in subscribed_channels:
            if Subscription.all_objects.filter(user=user, channel=channel).exists():
                Subscription.all_objects.filter(user=user, channel=channel).update(
                    deleted=False,
                    youtube_id=subscription_ids[channel.youtube_id]
                )
                new.add(Subscription.all_objects.filter(user=user, channel=channel).values_list("id", flat=True)[0])
                continue

            sub = Subscription(user=user, channel=channel, initial=initial,
                               youtube_id=subscription_ids[channel.youtube_id])
            try:
                sub.save()
                new.add(sub.id)
            except IntegrityError:
                pass

        if new_channels:
            LoadDataTask.delay()

        user.sync_subscriptions = False
        user.save()

        return {"new": list(new), "delete": list(delete)}


class LoadWatchedTask(Task):

    @staticmethod
    def get_watched_playlist_id(user):
        """
        :param  user: Which user
        :type user: CustomUser
        """
        if user.watched_playlist_id:
            return user.watched_playlist_id
        else:
            social = user.social_auth.get(provider='google-oauth2')

            try:
                response = requests.get(
                    'https://www.googleapis.com/youtube/v3/channels',
                    params={'access_token': social.extra_data['access_token'],
                            'part': 'contentDetails',
                            'mine': 'true'}  # max usable
                )
            except BaseException:
                client.captureException()
                raise YouTubeUnavailable

            try:
                data = response.json()
            except BaseException:
                client.captureException(extra={"response": response.content})
                return None

            try:
                user.watched_playlist_id = data['items'][0]["contentDetails"]["relatedPlaylists"]["watchHistory"]
                user.save()
            except KeyError:
                client.captureMessage(
                    "youtube.tasks.LoadWatchedTask.get_watched_playlist_id failed",
                    extra={
                        "response": data,
                    },
                    level=logging.ERROR,
                    tags={"youtube": "watch_history_id"})
                return None

            return user.watched_playlist_id

    def run(self, user_id):
        user = CustomUser.objects.get(id=user_id)

        playlist_id = self.get_watched_playlist_id(user)

        if not playlist_id:
            return {"success": False, "marked": [], "reason": "empty_playlist_id"}

        social = user.social_auth.get(provider='google-oauth2')

        params = {
            'access_token': social.extra_data['access_token'],
            'playlistId': playlist_id,
            'part': "contentDetails",
            'maxResults': 10
        }

        items = []

        last_id = user.last_watched_id
        seen_last_id = False
        first_id = None
        pages_loaded = 0

        while True:

            try:
                response = requests.get(
                    'https://www.googleapis.com/youtube/v3/playlistItems',
                    params=params
                )
            except BaseException:
                client.captureException()
                return {"success": False, "marked": [], "reason": "bad_response"}

            try:
                data = response.json()
            except BaseException:
                client.captureException(extra={"response": response.content})
                return {"success": False, "marked": [], "reason": "bad_response"}

            if 'error' in data and data['error']['code'] == 401:
                logger.info(data)
                return {"success": False, "marked": [], "reason": "expired_code"}

            for item in data["items"]:
                if not first_id:
                    first_id = item["id"]
                items.append(item["contentDetails"]["videoId"])
                if item["id"] == last_id:
                    seen_last_id = True

            if 'nextPageToken' in data and not seen_last_id and pages_loaded <= 10:
                params["pageToken"] = data["nextPageToken"]
            else:
                break

            pages_loaded += 1

        marked = []

        if len(items):
            user.last_watched_id = first_id
            user.save()

            marked = Video.objects.filter(
                youtube_id__in=items
            ).exclude(
                states__user=user,
            ).values_list("id", flat=True)

            marked = list(marked)

            states = []
            for video_id in marked:
                states.append(State(video_id=video_id, user_id=user_id, watched=True))

            State.objects.bulk_create(states)

        result = {"success": True, "marked": marked if len(marked) else None, "reason": ""}

        return result


class TheColbertTask(Task):
    """ BetterSubBox has a hard time syncing Colberts YouTube
        channel because it releases the videos with same timestamp
    """

    SYNC_LAST_X_DAYS = 7

    def __init__(self):
        self.channel: Channel = None

    def run(self, channel_id: int, days=SYNC_LAST_X_DAYS) -> Optional[Dict[str, Union[str, int]]]:
        try:
            self.channel = Channel.objects.get(id=channel_id)
        except (ObjectDoesNotExist, MultipleObjectsReturned):
            client.captureException()
            return

        params = {
            'key': settings.YOUTUBE_API_KEY,
            'channelId': self.channel.youtube_id,
            'part': 'id,snippet',
            'maxResults': 50,
            'order': 'date',
            'type': 'video'
        }

        response = None
        page = 1
        sync_log = SyncLog.objects.create(channel=self.channel)

        try:
            response = requests.get(
                'https://www.googleapis.com/youtube/v3/search',
                params=params
            )
            logger.info("Loaded page {}".format(page))
        except (requests.exceptions.ChunkedEncodingError, requests.exceptions.ConnectionError,
                requests.exceptions.SSLError) as e:
            client.captureMessage("youtube.tasks.TheColbertTask.run failed: {}".format(e),
                                  level=logging.INFO,
                                  extra={"response": response},
                                  tags={"youtube": "colbert_task"})
            return
        except BaseException:
            client.captureException()
            return

        data = response.json()

        sync_log.data = data
        sync_log.data.setdefault("items", [])

        last_video, counter = self.treat_data(data, sync_log)  # type: Optional[Video]

        if last_video is None:  # an error occurred
            sync_log.save()
            return {"channel": self.channel.title, "last_video": None}

        logger.debug("Last video was published at {}".format(last_video.published.isoformat()))

        while last_video.published > timezone.now() - relativedelta(days=days):
            try:
                params["pageToken"] = data["nextPageToken"]

                response = requests.get(
                    'https://www.googleapis.com/youtube/v3/search',
                    params=params
                )
                page += 1
                logger.info("Loaded page {}".format(page))
            except (requests.exceptions.ChunkedEncodingError, requests.exceptions.ConnectionError,
                    requests.exceptions.SSLError) as e:
                client.captureMessage("youtube.tasks.TheColbertTask.run failed: {}".format(e),
                                      level=logging.INFO,
                                      extra={"response": response},
                                      tags={"youtube": "colbert_task"})
                break
            except BaseException:
                client.captureException()
                break

            data = response.json()

            sync_log.data["items"] += data.get("items", [])

            last_video, x = self.treat_data(data, sync_log)

            if last_video is None:
                break

            logger.debug("Last video was published at {}".format(last_video.published.isoformat()))

            counter += x

        sync_log.save()

        return {"counter": counter, "channel": self.channel.title}

    def treat_data(self, _data, sync_log):
        # type: (Dict[str, Any], SyncLog) -> Tuple[Optional[Video], int]
        """ Parses the data in YouTube response
        """
        if 'items' not in _data:
            client.captureMessage("youtube.tasks.TheColbertTask.treat_data failed, items not in data",
                                  extra={"response": _data},
                                  level=logging.INFO,
                                  tags={"youtube": "colbert_task"})

            return None, 0

        videos = _data['items']

        last_video = None
        cnt = 0

        for i, video in enumerate(videos):
            if video['id']['kind'] == 'youtube#video':
                try:
                    new_vid = Video(youtube_id=video['id']['videoId'],
                                    channel=self.channel,
                                    published=parse_published(video['snippet']['publishedAt']),
                                    title=video['snippet']['title'])
                    new_vid.save()
                    last_video = new_vid
                    sync_log.add_video(video)
                    cnt += 1
                except IntegrityError:
                    if i == len(videos) - 1:
                        last_video = Video.objects.get(youtube_id=video['id']['videoId'])

        return last_video, cnt


class SyncTask(Task):

    def run(self, channel_id):
        # type: (int) -> None
        from apps.youtube.cron import GetNewVideosJob

        sync_job = GetNewVideosJob()

        try:
            channel = Channel.objects.get(id=channel_id)
        except ObjectDoesNotExist:
            return

        sync_job.get_new_channel_videos(channel)
