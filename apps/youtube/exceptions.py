# encoding=utf-8


class YouTubeUnavailable(Exception):
    pass


class InvalidCredentials(Exception):
    pass


class InsufficientPermission(Exception):
    pass
