# encoding=utf-8
import json
import logging
from collections import namedtuple
from datetime import date

import dateutil.parser
import isodate
import requests
from django.conf import settings
from django.db.models import F

from apps import redis, client
from apps.core.models import Counter
from apps.youtube.exceptions import YouTubeUnavailable, InvalidCredentials, InsufficientPermission

TreatedResult = namedtuple("TreatedTuple", ["counter", "end"])
SubscriptionSyncResult = namedtuple("SubscriptionSyncResult", ["new", "delete"])


def delete_subscription(user, youtube_id):
    """
    Send a request to YouTube to delete a subscription
    :param user: User
    :param youtube_id: YouTube ID of the subscription
    :return: Bool
    """
    social = user.social_auth.get(provider='google-oauth2')  # users access token
    try:
        deletion = requests.delete(
            'https://www.googleapis.com/youtube/v3/subscriptions',
            params={
                'access_token': social.extra_data['access_token'],
                'id': youtube_id
            }
        )
    except BaseException:
        client.captureException()
        raise YouTubeUnavailable

    if deletion.status_code == 204:
        return True
    else:
        try:
            data = deletion.json()
        except BaseException:
            data = deletion

        if data.get("error", {}).get("code") == 401:
            raise InvalidCredentials()
        elif data.get("error", {}).get("code") == 403:
            user.readonly_access = True
            user.save()
            raise InsufficientPermission()

        client.captureMessage("Subscription could not be deleted",
                              extra={
                                  "response": data,
                                  "user": user,
                                  "youtube_id": youtube_id
                              },
                              tags={"youtube": "delete_subscription"},
                              level=logging.WARNING)
        return False


def parse_published(string):
    """ Creates a datetime object from a string.
    :param string:
    :return: datetime instance
    """
    return dateutil.parser.parse(string)


def add_zero(integer):
    """ Pads the number with a leading zero
    :param integer: int
    :return: str with a leading zero
    """
    string = str(integer)
    if len(string) == 1:
        return "0%s" % (string,)
    else:
        return string


def parse_duration(orig_string):
    """
    Creates a nicer string from YouTube format:

    YOUTUBE DURATION FORMAT: The length of the video. The tag value is an ISO 8601 duration in the format PT#M#S,
    in which the letters PT indicate that the value specifies a period of time, and the letters M and S refer to
    length in minutes and seconds, respectively. The # characters preceding the M and S letters are both integers
    that specify the number of minutes (or seconds) of the video. For example, a value of PT15M51S indicates that the
    video is 15 minutes and 51 seconds long.

    PT1H5M33S is also an option

    :param orig_string: YouTube duration string
    :return: <min>:<sec> or <hour>:<min>:<sec>
    """
    if orig_string:
        total_seconds = int(isodate.parse_duration(orig_string).total_seconds())

        hours = total_seconds // 3600
        minutes = (total_seconds % 3600) // 60
        seconds = (total_seconds % 3600) % 60

        if not hours:
            return "%s:%s" % (add_zero(minutes), add_zero(seconds))
        else:
            return "%s:%s:%s" % (hours, add_zero(minutes), add_zero(seconds))


def parse_thumbnail(dictionary):
    """ Gets the best thumbnail available.
    :param dictionary:
    :return: url or None
    """
    sizes = ['maxres', 'standart', 'high', 'medium', 'default']

    for size in sizes:
        try:
            return dictionary[size]['url']
        except KeyError:
            pass


def parse_captions(string):
    """ Parses captions availability
    :param string: "true" or "false"
    :return: bool
    """
    if string == "true":
        return True
    else:
        return False


def load_extra_data(ids):
    """ Gets the extra details about videos that aren't stored in the database.
    :param ids: List of video youtube ids.
    :return: Dict of the extra datas, keyed by the youtubeid.
    """
    result = {}
    to_load = []

    # first find out if the video was already loaded in the last 24 hours and stored in redis.
    for pk in ids:
        if redis.exists("videodetail:" + pk):
            result[pk] = json.loads(redis.get("videodetail:" + pk).decode("utf-8  "))
        else:
            to_load.append(pk)

        if settings.USE_COUNTER:
            if Counter.objects.filter(video=pk, date=date.today()).exists():
                Counter.objects.filter(video=pk, date=date.today()).update(count=F('count') + 1)
            else:
                Counter.objects.create(video=pk, date=date.today(), count=1)

    # load the rest from youtube
    if len(to_load) > 0:

        while len(to_load) > 0:  # while there are still some ids left

            ids_string = ""
            i = 0
            while i < 50 and len(to_load) > 0:  # get fifty ids at a time to minimize requests (50 is max)
                pk = to_load.pop(0)

                if i == 0:
                    ids_string = "%s" % (pk,)
                else:
                    ids_string += ",%s" % (pk,)
                i += 1

            try:
                response = requests.get(
                    'https://www.googleapis.com/youtube/v3/videos',
                    params={'key': settings.YOUTUBE_API_KEY,
                            'id': ids_string,
                            'part': 'snippet,contentDetails'}
                )
            except BaseException:
                client.captureException()
                raise YouTubeUnavailable

            data = response.json()

            for video_data in data['items']:

                snippet = video_data['snippet']

                if (snippet["title"] == "Deleted video" and
                        snippet["description"] == "This video is unavailable." and
                        "contentDetails" not in video_data):
                    continue

                data = {
                    "description": snippet['description'],
                    "thumbnail": parse_thumbnail(snippet['thumbnails']),
                    "quality": video_data['contentDetails']['definition'].upper(),
                    "captions": parse_captions(video_data['contentDetails']['caption']),
                    "title": snippet['title'],
                    "duration": parse_duration(video_data['contentDetails']['duration'])
                }

                # save to redis (cachce)
                redis.setex("videodetail:" + video_data['id'], settings.CACHE_TIME, json.dumps(data))

                result[video_data['id']] = data

    return result
