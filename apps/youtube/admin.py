# encoding=utf-8
from __future__ import unicode_literals, print_function

from django.contrib import admin
from django.db.models import Count
from django.utils.translation import ugettext_lazy as _

from apps.youtube.forms import SyncLogForm
from apps.youtube.tasks import SyncTask, TheColbertTask
from .models import SyncLog


def run_new_sync_immediately(modeladmin, request, queryset):
    for obj in queryset:
        SyncTask().delay(obj.channel_id)


run_new_sync_immediately.short_description = _("Run new sync immediately")


def run_full_sync_immediately(modelamin, request, queryset):
    for obj in queryset:
        TheColbertTask().delay(obj.channel_id)


run_full_sync_immediately.short_description = _("Run full sync immediately")


class SomeVideosImportedFilter(admin.SimpleListFilter):

    title = _("Some videos were imported")
    parameter_name = "some_imported"

    def lookups(self, request, model_admin):
        return (
            ('yes', _("Only imported")),
        )

    def queryset(self, request, queryset):
        if self.value() == "yes":
            return queryset.annotate(num_videos=Count('videos')).filter(num_videos__gt=0)
        return queryset


class SyncLogAdmin(admin.ModelAdmin):

    form = SyncLogForm

    list_display = (
        'channel', 'time', 'videos_count'
    )

    actions = [run_new_sync_immediately, run_full_sync_immediately]

    list_filter = (
        SomeVideosImportedFilter,
        'channel',
    )

    def videos_count(self, obj):
        return obj.videos.count()


admin.site.register(SyncLog, SyncLogAdmin)
