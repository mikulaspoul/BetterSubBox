# encoding=utf-8
from __future__ import unicode_literals, print_function

from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.translation import ugettext_lazy as _


class SyncLogManager(models.Manager):

    def create(self, **kwargs):
        if settings.USE_SYNC_LOGS:
            return super(SyncLogManager, self).create(**kwargs)

        return SyncLog.DummySyncLog()


class SyncLog(models.Model):

    time = models.DateTimeField(auto_now_add=True, verbose_name=_("Time"))
    channel = models.ForeignKey("channel.Channel", on_delete=models.CASCADE, verbose_name=_("Channel"))
    videos = models.ManyToManyField("video.Video")
    data = JSONField(verbose_name=_("Data"), blank=True, default=dict)

    objects = SyncLogManager()

    class Meta:
        verbose_name = _("Sync Log")
        verbose_name_plural = _("Syncs Logs")

    def add_video(self, video):
        self.videos.add(video)

    class DummySyncLog:
        def add_video(self, video):
            pass

        def save(self):
            pass
