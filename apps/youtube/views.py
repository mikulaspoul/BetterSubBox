# encoding=utf-8
import json

import six
from celery.result import AsyncResult
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect, HttpResponse
from django.http.response import HttpResponseBadRequest, JsonResponse
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import View, TemplateView

from apps import client
from apps.channel.models import Subscription
from apps.youtube.exceptions import InsufficientPermission
from apps.youtube.exceptions import InvalidCredentials
from apps.youtube.tasks import GetOrRefreshSubscriptionsTask


class LoadingDataView(LoginRequiredMixin, TemplateView):
    """ Displays loading view - if not data is ready to display. """
    template_name = "loading_data.html"

    def get(self, request, *args, **kwargs):
        # data is already ready, redirects to index

        if not request.user.first_login and request.user.data_ready()['ready']:
            return HttpResponseRedirect(reverse_lazy("index"))

        if request.user.first_login:
            request.user.first_login = False
            request.user.save()

        GetOrRefreshSubscriptionsTask.delay(request.user.id)

        return super(self.__class__, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super(LoadingDataView, self).get_context_data(**kwargs)

        data["slides"] = [{
            "img": "/static/img/loading/states.png",
            "header": _("Mark video with states"),
            "text": _("You can set states to videos - watched, ignored and for later. In the left sidebar you can "
                      "then set if you want those videos to appear in your current feed (default is off). You can "
                      "also filter out just the videos marked with a specific state.")
        }, {
            "img": "/static/img/loading/player_and_layout.png",
            "header": _("Choose your layout and player"),
            "text": _("We have three different layouts on BetterSubBox - you can change "
                      "the current one right from the videos feed. Apart from different layouts we have several "
                      "options of players - you can open the YouTube page of the video, you can view it embedded "
                      "right here or you can open a player that's fullpage.")
        }, {
            "img": "/static/img/loading/favourites_and_homepage.png",
            "header": _("Set up favorite channels"),
            "text": _("Have channels you love and never want to miss an another video? Star them and they'll all "
                      "be just one click away from the menu. Or you can even set the favourites as your homepage "
                      "on BetterSubBox.")
        }, {
            "img": "/static/img/loading/groups.png",
            "header": _("Sort channels into groups"),
            "text": _("You can put channels into groups and then list just videos from the group.")
        }, {
            "img": "/static/img/loading/auto_ignore.png",
            "header": _("Set up patterns for automatic ignoring"),
            "text": _("Sometimes you don't like a series on a channel - well if the name is included in the title "
                      "you're in luck! You can setup patterns for specific channels and those videos will be "
                      "automatically ignored for you so you never have to worry about them.")
        }, {
            "img": "/static/img/loading/channels.png",
            "header": _("Lookup videos from a specific channel"),
            "text": _("You can list videos just from a specific channel and from there you can also manage it "
                      "you can (un)favourite it, go the the YouTube page and you can also unsubscribe.")
        }]

        return data


class YoutubeDownView(TemplateView):
    template_name = "youtube_down.html"


class SynchronizationSyncResultView(LoginRequiredMixin, View):

    def get(self, request):

        if "task_id" not in request.GET:
            return HttpResponseBadRequest()

        result = AsyncResult(request.GET["task_id"])

        if not result.ready():
            return JsonResponse({"ready": False})

        if result.failed():
            return JsonResponse({"ready": True, "failed": True})

        result_data = result.result

        data = {
            "ready": True,
            "deleted": None,
            "new": [],
            "delete": [],
            "failed": result_data.get("failed", False)
        }

        if not request.user.confirm_unsubscribe:
            if len(result_data["delete"]):
                query = Subscription.objects.filter(pk__in=result_data["delete"])
                deleted_channel_names = list(query.values_list("channel__title", flat=True))
                query.update(deleted=True)

                data["delete"] = [{"channel_name": x} for x in deleted_channel_names]

            data["deleted"] = True
        else:
            to_delete = Subscription.objects.filter(pk__in=result_data["delete"]).values_list("id", "channel__title")
            data["delete"] = [{"channel_name": x[1], "id": x[0]} for x in to_delete]
            data["deleted"] = False

        if len(result_data["new"]):
            new_channel_names = Subscription.objects.filter(pk__in=result_data["new"]).values_list("channel__title",
                                                                                                   flat=True)
            data["new"] = [{"channel_name": x} for x in new_channel_names]

        return JsonResponse(data)


class BulkSubscriptionDeleteView(LoginRequiredMixin, View):

    def post(self, request):
        data = request.POST.copy()

        ids = []
        for val in six.iterkeys(data):
            if "con_" in val:
                ids.append(int(val.replace("con_", "")))

        Subscription.objects.filter(user=request.user, pk__in=ids).update(deleted=True)

        return HttpResponse(json.dumps({"success": True}))


class SynchronizeView(LoginRequiredMixin, TemplateView):
    template_name = "synchronize.html"

    def __init__(self):
        super(SynchronizeView, self).__init__()
        self.task_id = None

    def get(self, request, *args, **kwargs):
        self.task_id = GetOrRefreshSubscriptionsTask.delay(request.user.id).id
        return super(SynchronizeView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        return {"task_id": self.task_id}


class UnsubscribeView(LoginRequiredMixin, View):

    def get(self, request, **kwargs):
        pk = kwargs.get("pk")

        try:
            pk = int(pk)
            sub = Subscription.objects.get(pk=pk)
            sub.delete()
            messages.success(request, _("The subscription was deleted."))
        except (ValueError, ObjectDoesNotExist):
            return HttpResponseRedirect(reverse_lazy("index"))
        except InvalidCredentials:
            messages.error(request, _("The YouTube credentials expired."))
        except InsufficientPermission:
            messages.error(request, _("You only have ready only permissions!"))
            return HttpResponseRedirect("/#/settings")
        except BaseException:
            client.captureException()

        return HttpResponseRedirect(reverse_lazy("index"))


class SynchronizeNewRequest(LoginRequiredMixin, View):

    def get(self, request):
        task_id = GetOrRefreshSubscriptionsTask.delay(request.user.id).id
        return JsonResponse({"task_id": task_id})


class LoadWatchedTaskResult(LoginRequiredMixin, View):

    def get(self, request):
        if "task_id" not in request.GET:
            return HttpResponseBadRequest()

        result = AsyncResult(request.GET["task_id"])

        if not result.ready():
            return JsonResponse({"ready": False, "failed": False})

        if result.failed():
            return JsonResponse({"ready": True, "failed": True})

        data = {
            "ready": True,
            "failed": False,
            "data": None,
            "reason": None
        }

        result = result.result

        if not result["success"]:
            data["failed"] = True
            data["reason"] = result["reason"]
        else:
            data["data"] = result["marked"]

        return JsonResponse(data)
