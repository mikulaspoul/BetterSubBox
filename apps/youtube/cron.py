# encoding=utf-8
import logging
import time
from typing import Dict, Any

import requests
import six
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.db.models import Count
from django.utils import timezone
from django.db import IntegrityError
from django_cron import CronJobBase, Schedule

from apps import client, logger, redis
from apps.channel.models import Channel, Subscription
from apps.channel.utils import ignore_videos
from apps.video.models import Video
from apps.youtube.models import SyncLog
from apps.youtube.tasks import TheColbertTask
from apps.youtube.utils import TreatedResult, parse_published


class UpdateChannelsJob(CronJobBase):
    RUN_AT_TIMES = ['3:15']

    schedule = Schedule(run_at_times=RUN_AT_TIMES)
    code = 'youtube.update_channels'

    @staticmethod
    def get_channels_details(ids):
        """
        Gets the details about channels that aren't stored in the database.
        :param ids: List of channel youtube ids.
        :return: Dict of the channel details, keyed by the youtubeid.
        """
        result = {}
        to_load = ids

        # load the rest from youtube
        if len(to_load) > 0:

            while len(to_load) > 0:  # while there are still some ids left

                ids_list = []
                # get fifty ids at a time to minimize requests (50 is max)
                while len(ids_list) < 50 and len(to_load):
                    ids_list.append(to_load.pop())

                ids_string = ",".join(ids_list)

                response = requests.get(
                    'https://www.googleapis.com/youtube/v3/channels',
                    params={
                        'key': settings.YOUTUBE_API_KEY,
                        'id': ids_string,
                        'part': 'snippet',
                        'maxResults': 50
                    }
                )
                data = response.json()

                if "items" not in data:
                    # typehinting workaround

                    client.captureMessage(
                        "youtube.tasks.UpdateChannelsJob.get_channels_details failed, items not in data",
                        extra={
                            "response": data,
                        },
                        level=logging.INFO,
                        tags={"youtube": "update_channels"})

                    return

                for channel_data in data['items']:
                    data = {
                        "title": channel_data['snippet']['title'],
                        "description": channel_data['snippet']['description'],
                        "thumbnail": channel_data['snippet']['thumbnails']['default']['url']
                    }

                    result[channel_data['id']] = data

        return result

    def do(self):
        """
        Updates the channel info if it has changed,
        :return: void
        """
        channels = list(Channel.objects.all())
        ids = []
        instances = {}

        for channel in channels:
            ids.append(channel.youtube_id)
            instances[channel.youtube_id] = channel

        results = self.get_channels_details(ids)

        for result in six.iterkeys(results):
            changes = 0
            if instances[result].title != results[result]["title"]:
                changes += 1

                instances[result].title = results[result]["title"]
            if instances[result].description != results[result]["description"]:
                changes += 1
                instances[result].description = results[result]["description"]
            if instances[result].thumbnail != results[result]["thumbnail"]:
                changes += 1
                instances[result].thumbnail = results[result]["thumbnail"]

            if changes:
                logger.info("Info of channel %s updated" % instances[result].title)
                instances[result].save()


class GetNewVideosJob(CronJobBase):
    RUN_EVERY = 15
    ALLOW_PARALLEL_RUNS = False

    schedule = Schedule(run_every_mins=RUN_EVERY)
    code = 'youtube.get_new_videos'

    @staticmethod
    def treat_data(_data, channel, sync_log):
        # type: (Dict[str, Any], Channel, SyncLog) -> TreatedResult
        """ Parses the data in YouTube response
        :param _data: dictionary
        :param channel: Which channel is being processed
        :return: (<number of loaded videos>, <finished_loading>)
        """
        local_counter = 0
        if 'items' not in _data:
            client.captureMessage("youtube.tasks.GetNewVideosJob.get_new_channel_videos failed, items not in data",
                                  extra={
                                      "response": _data,
                                  },
                                  level=logging.INFO,
                                  tags={"youtube": "new_videos"})

            return TreatedResult(local_counter, True)

        videos = _data['items']

        for video in videos:
            # only save the id in the database
            if video['id']['kind'] == 'youtube#video':
                try:
                    new_vid = Video(youtube_id=video['id']['videoId'],
                                    channel=channel,
                                    published=parse_published(video['snippet']['publishedAt']),
                                    title=video['snippet']['title'])
                    new_vid.save()
                    sync_log.add_video(new_vid)
                    local_counter += 1
                except IntegrityError:
                    # video already exists, that means that no newer videos can be found later
                    return TreatedResult(local_counter, True)

        return TreatedResult(local_counter, False)

    @staticmethod
    def new_id_in_data(_data):
        if "items" not in _data:
            return False

        for video in _data["items"]:
            if video['id']['kind'] == 'youtube#video':
                if Video.objects.filter(youtube_id=video['id']['videoId']).exists():
                    return True

        return False

    def get_new_channel_videos(self, channel):
        """ Loads new videos from the channel
        :param channel: Channel instance
        :return: Loaded videos
        """
        counter = 0
        upload_times_similar_loop_completed = False
        sync_log = SyncLog.objects.create(channel=channel)

        params = {
            'key': settings.YOUTUBE_API_KEY,
            'channelId': channel.youtube_id,
            'part': 'id,snippet',
            'maxResults': 1,
            'order': 'date',
            'type': 'video'
        }

        response = None

        while True:
            try:
                response = requests.get(
                    'https://www.googleapis.com/youtube/v3/search',
                    params=params
                )
            except (requests.exceptions.ChunkedEncodingError, requests.exceptions.ConnectionError,
                    requests.exceptions.SSLError) as e:
                client.captureMessage("youtube.tasks.GetNewVideosJob.get_new_channel_videos failed: {}".format(e),
                                      level=logging.INFO,
                                      extra={"response": response},
                                      tags={"youtube": "new_videos"})
                return counter
            except BaseException:
                client.captureException()
                return counter

            data = response.json()

            if not channel.upload_times_similar or upload_times_similar_loop_completed:
                break
            else:
                if not self.new_id_in_data(data):
                    break

                params["maxResults"] = min(50, max(int(channel.videos_per_day) * 3, 10))
                time.sleep(6)  # 5 seconds is the threshold on similarity, so let's wait 6 seconds
                upload_times_similar_loop_completed = True

        sync_log.data = data
        sync_log.data.setdefault("items", [])

        # only basic details are saved, other details are added in the data_loader script
        result = self.treat_data(data, channel, sync_log)
        counter += result.counter

        # added > 10 videos, check further until return
        while 'nextPageToken' in data and not result.end:
            try:
                params["pageToken"] = data["nextPageToken"]

                response = requests.get(
                    'https://www.googleapis.com/youtube/v3/search',
                    params=params
                )
            except (requests.exceptions.ChunkedEncodingError, requests.exceptions.ConnectionError,
                    requests.exceptions.SSLError) as e:
                client.captureMessage("youtube.tasks.GetNewVideosJob.get_new_channel_videos failed: {}".format(e),
                                      level=logging.INFO,
                                      extra={"response": response},
                                      tags={"youtube": "new_videos"})
                break

            data = response.json()

            sync_log.data["items"] += data.get("items", [])

            result = self.treat_data(data, channel, sync_log)
            counter += result.counter
            if result.end:
                break

        sync_log.save()

        if counter and channel.upload_times_similar:
            TheColbertTask().apply_async(args=[channel.id], countdown=10 * 60)

            if "Colbert" in channel.title:  # FML it's still not fucking working
                TheColbertTask().apply_async(args=[channel.id], countdown=20 * 60)
                TheColbertTask().apply_async(args=[channel.id], countdown=30 * 60)
                TheColbertTask().apply_async(args=[channel.id], countdown=60 * 60)
                TheColbertTask().apply_async(args=[channel.id], countdown=90 * 60)

        return counter

    def do(self):
        """ Loads new videos. """
        counter = 0

        priority = redis.get("new_videos:next_priority") or settings.NUMBER_OF_PRIORITIES

        if not isinstance(priority, int):
            priority = int(priority)

        if priority == 1:
            next_priority = settings.NUMBER_OF_PRIORITIES
        else:
            next_priority = priority - 1

        redis.set("new_videos:next_priority", next_priority)

        # only with loaded initial data to prevent clashes
        for channel in Channel.objects.filter(initial_data_loaded=True, priority__gte=priority):
            counter += self.get_new_channel_videos(channel)

        if counter:
            logger.info("Loaded %s new videos" % counter)

        # auto ignore the stuff user want to ignore
        ignore_videos()


class DeleteUnusedChannelsJob(CronJobBase):
    RUN_AT_TIMES = ["3:10"]
    RETRY_AFTER_FAILURE_MINS = 5

    schedule = Schedule(run_at_times=RUN_AT_TIMES, retry_after_failure_mins=RETRY_AFTER_FAILURE_MINS)
    code = 'youtube.delete_unused_channels'

    def do(self):
        """
        Deletes channels which have no subscriptions.
        :return: void
        """
        for channel in Channel.objects.filter(subscriptions__isnull=True):
            try:
                msg = "Deleted channel %s, no subscriptions..." % channel.title

                # the default manager excludes these
                Subscription.all_objects.filter(channel=channel, deleted=True).delete()

                channel.delete()
                logger.info(msg)
            except BaseException:
                client.captureException()


class DeleteOldSynLogsJob(CronJobBase):

    RUN_AT_TIMES = ["3:15"]

    schedule = Schedule(run_at_times=RUN_AT_TIMES)
    code = 'youtube.delete_old_sync_logs'

    def do(self):
        SyncLog.objects.annotate(num_videos=Count('videos')).filter(
            num_videos__gt=0,
            time__lte=timezone.now() - relativedelta(days=settings.SYNCLOG_KEEP_TIME)
        ).delete()
