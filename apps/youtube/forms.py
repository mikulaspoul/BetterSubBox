# encoding=utf-8
from __future__ import unicode_literals, print_function

from django import forms
from jsoneditor.forms import JSONEditor

from apps.youtube.models import SyncLog


class SyncLogForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(SyncLogForm, self).__init__(*args, **kwargs)

        if self.instance.pk:
            self.fields["videos"].queryset = self.fields["videos"].queryset.filter(
                pk__in=self.instance.videos.all()
            )

    class Meta:
        model = SyncLog
        exclude = [""]
        widgets = {
            "data": JSONEditor
        }
