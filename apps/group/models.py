# encoding=utf-8
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify

from apps.channel.models import Channel


class Group(models.Model):
    """ Database representation of groups. """

    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("User"),
                             on_delete=models.CASCADE)
    name = models.CharField(max_length=24, verbose_name=_("Name"))
    slug = models.SlugField(max_length=255, verbose_name=_("Slug"), blank=True, unique=True)
    exclude_from_feed = models.BooleanField(default=False, verbose_name=_("Exclude from feed"),
                                            help_text=_("When checked, the channels present in this "
                                                        "group are excluded from the main feed."))

    def save(self, **kwargs):
        if self.id:
            self.make_slug()
            must_after_first_save = False
        else:
            must_after_first_save = True

        super(self.__class__, self).save(**kwargs)

        if must_after_first_save:
            self.make_slug()
            super(self.__class__, self).save(**kwargs)

    def delete(self, *args, **kwargs):
        """ Memberships are also deleted on delete. """
        self.members.all().delete()
        super(self.__class__, self).delete()

    def make_slug(self):
        self.slug = slugify("%s %s" % (self.id, self.name))

    def __str__(self):
        return u"Group %s" % self.name

    class Meta:
        verbose_name = _("Group")
        verbose_name_plural = _("Groups")


class GroupMembership(models.Model):
    """ Database representation of membership to a group. """
    group = models.ForeignKey(Group, verbose_name=_("Group"), related_name="members",
                              on_delete=models.CASCADE)
    channel = models.ForeignKey(Channel, verbose_name=_("Channel"), related_name="groups",
                                on_delete=models.CASCADE)

    class Meta:
        unique_together = ("group", "channel")  # disallows duplicates
