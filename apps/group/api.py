# encoding=utf-8
from tastypie.authentication import SessionAuthentication
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from tastypie.validation import Validation

from apps.channel.models import Channel
from apps.core.api import CustomForeignKey, CustomModelResource, ReadOnlyAuthorization
from apps.group.models import Group, GroupMembership


class GroupAuthorization(ReadOnlyAuthorization):
    """
    User can ready, update, create and delete only his groups.
    """

    def create_detail(self, object_list, bundle):
        return True

    def update_list(self, object_list, bundle):
        return object_list

    def update_detail(self, object_list, bundle):
        return True

    def delete_list(self, object_list, bundle):
        return object_list

    def delete_detail(self, object_list, bundle):
        return True


class GroupValidation(Validation):
    """ Validates users input. """

    def is_valid(self, bundle, request=None):
        errors = {}

        if 'name' not in bundle.data:
            errors['name'] = ["You have set the name."]

        if 'user' not in bundle.data:
            errors['user'] = ["You have to set user"]
        elif bundle.data['user'] != request.user.pk:
            errors['user'] = ["You can't create/edit groups of other users"]

        return errors


class GroupResource(CustomModelResource):
    """ Group API """
    user = CustomForeignKey("apps.custom_user.api.CustomUserResource", "user", full=False, pk_only=True)

    def get_object_list(self, request):
        return super(GroupResource, self).get_object_list(request).filter(user_id=request.user.pk)

    class Meta:
        queryset = Group.objects.all()
        resource_name = "group"
        authentication = SessionAuthentication()
        authorization = GroupAuthorization()
        validation = GroupValidation()
        always_return_data = True
        list_allowed_methods = ['get', 'post']
        filtering = {
            "id": ALL  # allows filtering by ID
        }
        ordering = ["name"]  # allows ordering by name


class GroupMembershipAuthorization(ReadOnlyAuthorization):
    """ User can ready, update, create and delete only members of his groups. """

    def create_detail(self, object_list, bundle):
        return True

    def update_list(self, object_list, bundle):
        return object_list

    def update_detail(self, object_list, bundle):
        return True

    def delete_list(self, object_list, bundle):
        return object_list

    def delete_detail(self, object_list, bundle):
        return True


class GroupMembershipValidation(Validation):
    """ Validates users input. """

    def is_valid(self, bundle, request=None):
        errors = {}

        # read error messages
        if 'group' not in bundle.data:
            errors['group'] = ["You have set the group."]
        else:
            group = Group.objects.get(pk=bundle.data['group'])
            if not group.user == bundle.request.user:
                errors['group'] = ["You can't create memberships for group of other people"]

        if 'channel' not in bundle.data:
            errors['channel'] = ["You have to set channel"]
        else:
            channel = Channel.objects.get(pk=bundle.data['channel'])
            if not channel.subscriptions.filter(user=bundle.request.user).exists():
                errors['channel'] = ["You can't create memberships for groups you aren't subscribed to"]
            if GroupMembership.objects.filter(group__pk=bundle.data['group'],
                                              channel__pk=bundle.data['channel']).exists():
                errors['channel'] = ["You cannot create duplicate memberships."]

        return errors


class GroupMembershipResource(CustomModelResource):
    """ Group Membership API. """
    group = CustomForeignKey(GroupResource, "group", full=False, pk_only=True)

    # full appends info about channel so no extra request is needed to display name
    channel = CustomForeignKey("apps.channel.api.ChannelResource", "channel", full=True, pk_only=True)

    def get_object_list(self, request):
        return super(GroupMembershipResource, self).get_object_list(request).filter(group__user_id=request.user.pk)

    class Meta:
        queryset = GroupMembership.objects.select_related("channel").all()
        resource_name = "group_membership"
        authentication = SessionAuthentication()
        authorization = GroupMembershipAuthorization()
        validation = GroupMembershipValidation()
        always_return_data = True
        filtering = {
            "group": ALL_WITH_RELATIONS  # allowes filter by group
        }
