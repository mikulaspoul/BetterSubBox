# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('group', '0002_auto_20141226_1537'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='groupmembership',
            unique_together=set([('group', 'channel')]),
        ),
    ]
