# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


from django.utils.text import slugify

def set_slugs(apps, schema_editor):
    Group = apps.get_model("group", "Group")

    for group in Group.objects.all():
        group.slug = slugify("%s %s" % (group.id, group.name))
        group.save()

class Migration(migrations.Migration):

    dependencies = [
        ('group', '0004_group_slug'),
    ]

    operations = [
         migrations.RunPython(code=set_slugs)
    ]
