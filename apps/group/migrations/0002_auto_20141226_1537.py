# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('group', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='name',
            field=models.CharField(max_length=24, verbose_name='Name'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='groupmembership',
            name='channel',
            field=models.ForeignKey(related_name='groups', verbose_name='Channel', to='channel.Channel', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='groupmembership',
            name='group',
            field=models.ForeignKey(related_name='members', verbose_name='Group', to='group.Group', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
