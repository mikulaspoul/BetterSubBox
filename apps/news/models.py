# encoding=utf-8
from django.db import models
from django.conf import settings


class News(models.Model):

    title = models.CharField(max_length=50)
    text = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    seen_by = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True)

    def __str__(self):
        return "{} - {}".format(self.title, self.created)
