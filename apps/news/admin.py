# encoding=utf-8
from django.contrib import admin
from apps.news.models import News


class NewsAdmin(admin.ModelAdmin):
    pass


admin.site.register(News, NewsAdmin)
