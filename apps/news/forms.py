# encoding=utf-8
from django import forms

from apps.news.models import News


class NewsForm(forms.ModelForm):
    seen_by = forms.SelectMultiple()

    class Meta:
        model = News
        fields = "__all__"
