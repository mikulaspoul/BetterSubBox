# encoding=utf-8
import json
import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.http import Http404

from apps.news.models import News


class ReadNews(LoginRequiredMixin, View):
    """ Marks news as viewed by the user, via the "news" list in POST.
    """
    @csrf_exempt
    def post(self, request):
        data = None

        try:
            data = json.loads(request.body)
            news_ids = [int(x) for x in data["news"]]
        except (ValueError, KeyError):
            if data is not None:
                logging.error("News could not be loaded, ids %s" % data["news"])
            raise Http404

        news = News.objects.filter(id__in=news_ids)

        user = request.user

        for new in news:
            new.seen_by.add(user)

        return JsonResponse({"status": True})
