# encoding=utf-8
from typing import List, Dict, Union

from apps.custom_user.models import CustomUser
from apps.news.models import News


def get_news_to_display(user: CustomUser) -> List[Dict[str, Union[id, str]]]:
    result = []
    for news in News.objects.filter(created__gt=user.created).exclude(seen_by=user).order_by("-created"):
        result.append({
            "id": news.id,
            "title": news.title,
            "text": news.text
        })
    return result
