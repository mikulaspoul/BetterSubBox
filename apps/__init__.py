# encoding=utf-8
import os
import logging
import pprint

from redis import StrictRedis

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')

from raven.contrib.django.raven_compat.models import client as clnt  # noqa
from raven.contrib.django.client import DjangoClient  # noqa
from django.conf import settings  # noqa

from .celery_config import app as celery_app  # NOQA

redis = StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT,
                    db=settings.REDIS_DB, password=settings.REDIS_PASSWORD)
printer = pprint.PrettyPrinter(indent=2)
logger = logging.getLogger(__name__)
client = clnt  # type: DjangoClient
