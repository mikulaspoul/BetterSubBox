# encoding=utf-8
from django.db import models


class Counter(models.Model):

    video = models.CharField(max_length=100)
    date = models.DateField()
    count = models.IntegerField(default=0)
