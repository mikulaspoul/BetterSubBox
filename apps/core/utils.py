# encoding=utf-8
import json

from datetime import date
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.db.models import QuerySet
from django.contrib.messages import get_messages

from apps import redis


def cache_count(queryset: QuerySet, name: str) -> int:
    redis_name = "count:{}".format(name)

    res = redis.get(redis_name)
    if res is not None:
        return res

    res = queryset.count()

    redis.setex(redis_name, settings.COUNT_CACHE_TIME, res)

    return res


def date_range_iterator(date_from: date, date_to: date):

    while date_from != date_to:
        yield date_from
        date_from += relativedelta(days=1)

    yield date_to


def serialize_messages(request):
    messages = get_messages(request)

    jsonified = []

    for message in messages:
        jsonified.append({
            "text": message.message,
            "title": message.level_tag
        })

    return json.dumps(jsonified)
