# encoding=utf-8
from apps import logger
from apps.custom_user.models import CustomUser
from apps.core.exceptions import AccountSwitchNotAllowed


def disable_multiple_accounts(backend, user, response, *args, **kwargs):
    q = user.social_auth.filter(provider='google-oauth2')
    if q.exists():
        social = q.get()

        if social.uid != kwargs.get("uid"):
            raise AccountSwitchNotAllowed()


def log(backend, user, response, *args, **kwargs):
    logger.info("%s has logged in" % user)


def undelete(backend, user, response, *args, **kwargs):
    if user.disconnected is not None:
        CustomUser.objects.filter(pk=user.pk).update(disconnected=None)
