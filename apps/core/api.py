# encoding=utf-8

import logging
from copy import copy

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.signals import got_request_exception
from django.http import HttpResponseForbidden
from django.http import HttpResponseNotFound, Http404
from django.utils import six
from django.utils.cache import patch_cache_control, patch_vary_headers
from tastypie import fields
from tastypie import http
from tastypie.api import Api
from tastypie.authorization import ReadOnlyAuthorization as Authorization
from tastypie.bundle import Bundle
from tastypie.exceptions import NotFound, BadRequest, Unauthorized, ApiFieldError
from tastypie.resources import ModelResource
from tastypie.resources import sanitize

from apps.core.http import HttpResponseServiceUnavailable
from apps.youtube.exceptions import YouTubeUnavailable

__all__ = [
    "CustomApi",
    "CustomModelResource",
    "CustomForeignKey",
    "ReadOnlyAuthorization",
]


# If ``csrf_exempt`` isn't present, stub it.
try:
    from django.views.decorators.csrf import csrf_exempt
except ImportError:
    def csrf_exempt(func):
        return func


class CustomApi(Api):
    """ Customized API class, where you can't view list of api resources. """

    def top_level(self, request, api_name=None):
        return HttpResponseForbidden()


class CustomModelResource(ModelResource):
    """ Customized ModelResource class, where you can't visit schema. """

    def get_schema(self, request, **kwargs):
        return HttpResponseForbidden()

    def wrap_view(self, view):
        """
        Wraps methods so they can be called in a more functional way as well
        as handling exceptions better.

        Note that if ``BadRequest`` or an exception with a ``response`` attr
        are seen, there is special handling to either present a message back
        to the user or return the response traveling with the exception.
        """
        @csrf_exempt
        def wrapper(request, *args, **kwargs):
            try:
                callback = getattr(self, view)
                response = callback(request, *args, **kwargs)

                # Our response can vary based on a number of factors, use
                # the cache class to determine what we should ``Vary`` on so
                # caches won't return the wrong (cached) version.
                varies = getattr(self._meta.cache, "varies", [])

                if varies:
                    patch_vary_headers(response, varies)

                if self._meta.cache.cacheable(request, response):
                    if self._meta.cache.cache_control():
                        # If the request is cacheable and we have a
                        # ``Cache-Control`` available then patch the header.
                        patch_cache_control(response, **self._meta.cache.cache_control())

                if request.is_ajax() and not response.has_header("Cache-Control"):
                    # IE excessively caches XMLHttpRequests, so we're disabling
                    # the browser cache here.
                    # See http://www.enhanceie.com/ie/bugs.asp for details.
                    patch_cache_control(response, no_cache=True)

                return response
            except (BadRequest, fields.ApiFieldError) as e:
                data = {"error": sanitize(e.args[0]) if getattr(e, 'args') else ''}
                return self.error_response(request, data, response_class=http.HttpBadRequest)
            except ValidationError as e:
                data = {"error": sanitize(e.messages)}
                return self.error_response(request, data, response_class=http.HttpBadRequest)
            except YouTubeUnavailable:
                return self.error_response(request,
                                           {"error": "YouTube is unavailable"},
                                           response_class=HttpResponseServiceUnavailable)
            except Exception as e:
                if hasattr(e, 'response'):
                    return e.response

                # A real, non-expected exception.
                # Handle the case where the full traceback is more helpful
                # than the serialized error.
                if settings.DEBUG and getattr(settings, 'TASTYPIE_FULL_DEBUG', False):
                    raise

                # Re-raise the error to get a proper traceback when the error
                # happend during a test case
                if request.META.get('SERVER_NAME') == 'testserver':
                    raise

                # Rather than re-raising, we're going to things similar to
                # what Django does. The difference is returning a serialized
                # error message.
                return self._handle_500(request, e)

        return wrapper

    def _handle_500(self, request, exception):
        import traceback
        import sys
        the_trace = '\n'.join(traceback.format_exception(*(sys.exc_info())))
        response_class = http.HttpApplicationError
        response_code = 500

        not_found_exceptions = (NotFound, ObjectDoesNotExist, Http404)

        if isinstance(exception, not_found_exceptions):
            response_class = HttpResponseNotFound
            response_code = 404

        if settings.DEBUG:
            if settings.LOG_DEBUG:
                log = logging.getLogger('django.request.tastypie')
                log.error('Internal Server Error: %s' % request.path, exc_info=True,
                          extra={'status_code': response_code, 'request': request})

            data = {
                "error_message": sanitize(six.text_type(exception)),
                "traceback": the_trace,
            }
            return self.error_response(request, data, response_class=response_class)

        # When DEBUG is False, send an error message to the admins (unless it's
        # a 404, in which case we check the setting).
        send_broken_links = getattr(settings, 'SEND_BROKEN_LINK_EMAILS', False)

        if not response_code == 404 or send_broken_links:
            log = logging.getLogger('django.request.tastypie')
            log.error('Internal Server Error: %s' % request.path, exc_info=True,
                      extra={'status_code': response_code, 'request': request})

        # Send the signal so other apps are aware of the exception.
        got_request_exception.send(self.__class__, request=request)

        # Prep the data going out.
        data = {
            "error_message": getattr(settings, 'TASTYPIE_CANNED_ERROR',
                                     "Sorry, this request could not be processed. Please try again later."),
        }
        return self.error_response(request, data, response_class=response_class)

    def get_via_key(self, key, request=None):
        """
        Populates the resource via a ``obj_get``.

        Optionally accepts a ``request``.
        """
        bundle = self.build_bundle(request=request)
        return self.obj_get(bundle=bundle, pk=key)

    def detail_uri_kwargs(self, bundle_or_obj):
        """
        Given a ``Bundle`` or an object (typically a ``Model`` instance),
        it returns the extra kwargs needed to generate a detail URI.

        By default, it uses the model's ``pk`` in order to create the URI.
        """
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs[self._meta.detail_uri_name] = self.get_obj_id(bundle_or_obj.obj)
        else:
            kwargs[self._meta.detail_uri_name] = self.get_obj_id(bundle_or_obj)

        return kwargs

    def get_obj_id(self, obj):
        if isinstance(obj, int):
            return obj
        else:
            return getattr(obj, self._meta.detail_uri_name)

    def dehydrate(self, bundle):
        """ Dehydrate full resources if set in url
        """
        for key in bundle.request.GET.keys():

            if key.startswith("make_") and key.endswith("_full"):

                model_name = key.replace("make_", "").replace("_full", "")

                if not hasattr(self, model_name):
                    continue

                model = getattr(self, model_name)
                old_full = model.full
                model.full = True
                bundle.data[model_name] = model.dehydrate(bundle)

                try:
                    new_data = []
                    for bun in bundle.data[model_name]:
                        try:
                            if self.fields[model_name].to_class._meta.authorization.read_detail([], bun):
                                new_data.append(bun)
                        except BaseException:
                            pass
                    bundle.data[model_name] = new_data
                except BaseException:
                    try:

                        self.fields[model_name].to_class._meta.authorization.read_detail([], bundle.data[model_name])
                    except BaseException:
                        del bundle.data[model_name]

                model.full = old_full

        return bundle


class CustomRelatedField(fields.RelatedField):
    """ Customized RelatedField field, that returns just the primary key, not resource uri. """

    def __init__(self, *args, **kwargs):
        self.pk_only = kwargs.pop("pk_only", False)

        super(CustomRelatedField, self).__init__(*args, **kwargs)

    def resource_from_key(self, fk_resource, key, request=None, related_obj=None, related_name=None):
        """
        Given a PK is provided, the related resource is attempted to be
        loaded based on the key
        """
        err_msg = "Could not find the provided %s object with the key '%s'." % (fk_resource._meta.resource_name, key,)

        if not key:
            raise ApiFieldError(err_msg)

        try:

            obj = fk_resource.get_via_key(key, request=request)
            bundle = fk_resource.build_bundle(
                obj=obj,
                request=request
            )
            return fk_resource.full_dehydrate(bundle)
        except ObjectDoesNotExist:
            raise ApiFieldError(err_msg)

    def build_related_resource(self, value, request=None, related_obj=None, related_name=None):
        """
        Returns a bundle of data built by the related resource, usually via
        ``hydrate`` with the data provided.

        Accepts either a URI, a data dictionary (or dictionary-like structure),
        an object with a ``pk`` or the primary key itself (if ``pk_only`` is set to ``True``)
        """
        fk_resource = self.to_class()
        kwargs = {
            'request': request,
            'related_obj': related_obj,
            'related_name': related_name,
        }

        if isinstance(value, Bundle):
            # Already hydrated, probably nested bundles. Just return.
            return value
        elif not self.pk_only and isinstance(value, six.string_types):
            # We got a URI. Load the object and assign it.
            return self.resource_from_uri(fk_resource, value, **kwargs)
        elif self.pk_only and (isinstance(value, six.string_types) or isinstance(value, six.integer_types)):
            # We've got the primary key
            return self.resource_from_key(fk_resource, value, **kwargs)
        elif isinstance(value, dict):
            # We've got a data dictionary.
            # Since this leads to creation, this is the only one of these
            # methods that might care about "parent" data.
            return self.resource_from_data(fk_resource, value, **kwargs)
        elif hasattr(value, 'pk') or hasattr(value, "id"):
            # We've got an object with a primary key.
            return self.resource_from_pk(fk_resource, value, **kwargs)
        else:
            raise ApiFieldError("The '%s' field was given data that was not a URI, "
                                "not a dictionary-alike, not the pk itself and "
                                "does not have a 'pk' attribute: %s." % (self.instance_name, value))

    def dehydrate_related(self, bundle, related_resource, for_list=True):
        """
        Based on the ``full_resource``, returns either the endpoint or the data
        from ``full_dehydrate`` for the related resource.
        """
        should_dehydrate_full_resource = self.should_full_dehydrate(bundle, for_list=for_list)

        if not should_dehydrate_full_resource:
            # Be a good netizen.
            if not self.pk_only:
                return related_resource.get_resource_uri(bundle)
            else:
                return related_resource.detail_uri_kwargs(bundle)[related_resource._meta.detail_uri_name]
        else:
            # ZOMG extra data and big payloads.
            bundle = related_resource.build_bundle(
                obj=bundle.obj,
                request=bundle.request,
                objects_saved=bundle.objects_saved
            )
            return related_resource.full_dehydrate(bundle)


class CustomForeignKey(CustomRelatedField, fields.ToOneField):

    def dehydrate(self, bundle, for_list=True):
        foreign_obj = attr = previous_obj = None

        if callable(self.attribute):
            previous_obj = bundle.obj
            foreign_obj = self.attribute(bundle)
        elif isinstance(self.attribute, six.string_types):
            foreign_obj = bundle.obj

            attrs = copy(self._attrs)

            # to prevent getting the whole object
            if not self.full:
                attrs[-1] += "_id"

            for attr in attrs:
                previous_obj = foreign_obj
                try:
                    foreign_obj = getattr(foreign_obj, attr, None)
                except ObjectDoesNotExist:
                    foreign_obj = None

        if not foreign_obj:
            if not self.null:
                if callable(self.attribute):
                    raise ApiFieldError("The related resource for resource %s could not be found." % previous_obj)
                else:
                    raise ApiFieldError("The model '%r' has an empty attribute '%s' and doesn't allow a null value." % (
                        previous_obj, attr))
            return None

        fk_resource = self.get_related_resource(foreign_obj)
        fk_bundle = Bundle(obj=foreign_obj, request=bundle.request)
        return self.dehydrate_related(fk_bundle, fk_resource, for_list=for_list)


class ReadOnlyAuthorization(Authorization):

    def create_list(self, object_list, bundle):
        raise Unauthorized()

    def create_detail(self, object_list, bundle):
        raise Unauthorized()

    def update_list(self, object_list, bundle):
        raise Unauthorized()

    def update_detail(self, object_list, bundle):
        raise Unauthorized()

    def delete_list(self, object_list, bundle):
        raise Unauthorized()

    def delete_detail(self, object_list, bundle):
        raise Unauthorized()
