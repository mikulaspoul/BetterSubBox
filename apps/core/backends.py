# encoding=utf-8
from django.conf import settings
from social_core.backends.google import GoogleOAuth2


class CustomGoogleOAuth2(GoogleOAuth2):

    def get_scope(self):
        scope = super(CustomGoogleOAuth2, self).get_scope()

        if "write_access" in self.data:
            scope.append(settings.YOUTUBE_WRITE_SCOPE)

        return scope
