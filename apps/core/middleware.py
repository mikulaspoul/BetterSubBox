# encoding=utf-8
from django.contrib import messages
from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.template.response import SimpleTemplateResponse
from django.urls import reverse
from django.utils.functional import SimpleLazyObject
from django.utils.translation import ugettext_lazy as _
from social_core.exceptions import AuthCanceled, AuthAlreadyAssociated

from apps.core.exceptions import AccountSwitchNotAllowed
from apps.youtube.exceptions import YouTubeUnavailable


def fake_get_user(request):
    if not hasattr(request, '_cached_user'):
        from apps.custom_user.models import CustomUser
        request._cached_user = CustomUser.objects.get(pk=24)
    return request._cached_user


class FakeAuth(object):
    def process_request(self, request):
        request.user = SimpleLazyObject(lambda: fake_get_user(request))


class ConnectionErrorMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        if hasattr(request, "user"):
            if request.user.is_authenticated:
                if not request.user.readonly_access and not request.COOKIES.get("write_access"):
                    response.set_cookie("write_access", 1, max_age=365 * 24 * 60 * 60)
                elif request.user.readonly_access and request.COOKIES.get("write_access"):
                    response.delete_cookie("write_access")

        return response

    def process_exception(self, request, exception):
        if isinstance(exception, YouTubeUnavailable):
            return SimpleTemplateResponse("youtube_down.html", status=503)
        elif isinstance(exception, AuthCanceled):
            messages.error(request, _("Sign in cancelled."))
            return HttpResponseRedirect(reverse("login"))
        elif isinstance(exception, (AccountSwitchNotAllowed, AuthAlreadyAssociated)):
            logout(request)
            return HttpResponseRedirect(reverse("account_switched"))
