# encoding=utf-8
from django.conf import settings
from apps.core import utils


def debug(request):
    return {"debug": "true" if settings.DEBUG else "false"}


def write_access_cookie(request):
    return {
        "write_access_cookie": bool(request.COOKIES.get("write_access")) or (
            request.user.is_authenticated and not request.user.readonly_access
        )
    }


def serialize_messages(request):
    return {
        "serialized_messages": utils.serialize_messages(request)
    }
