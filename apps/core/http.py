from django.http import HttpResponseServerError


class HttpResponseServiceUnavailable(HttpResponseServerError):
    status_code = 503
