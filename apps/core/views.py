# encoding=utf-8
import json
import logging
from datetime import date

from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib.auth import logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import TemplateView, View
from typing import Dict, List, Optional, Union

from apps import redis, client
from apps.channel.models import Channel, Subscription
from apps.core.utils import cache_count, date_range_iterator
from apps.custom_user.api import CustomUserResource
from apps.custom_user.models import CustomUser
from apps.news.utils import get_news_to_display
from apps.video.models import Video, State
from apps.youtube.tasks import LoadWatchedTask


class LoginView(TemplateView):
    """
    Displays login page with button for login.
    """
    template_name = "login.html"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.sync_subscriptions and not request.user.first_login:
                return HttpResponseRedirect(reverse_lazy("synchronize"))

            # if data is not ready -> loading data view
            if request.user.first_login or not request.user.data_ready()['ready']:
                return HttpResponseRedirect(reverse_lazy("loading_data"))

            # we want to load only after login, but also we want the id to have the result handy
            if request.user.load_watched:
                redis.set("load_watched:{}".format(request.user.id), "true", 15)

            return HttpResponseRedirect(reverse_lazy("index"))  # no need of login, user is logged in

        # user is not logged in -> display login page
        return super(self.__class__, self).dispatch(request, *args, **kwargs)


class LogoutView(LoginRequiredMixin, View):
    """ Logs out user. """
    def dispatch(self, request, *args, **kwargs):
        logout(request)
        return HttpResponseRedirect(reverse_lazy("index"))


class IndexView(TemplateView):

    def __init__(self):
        super(IndexView, self).__init__()
        self.request = None  # filled later
        self.load_watched_task: str = None

    def get_template_names(self):
        if self.request.user.is_authenticated:
            return "index.html"
        else:
            return "landing.html"

    def dispatch(self, request, *args, **kwargs):
        # if data is not ready -> loading data view
        if request.user.is_authenticated:
            if not request.user.data_ready()['ready']:
                return HttpResponseRedirect(reverse_lazy("loading_data"))

            if self.request.user.load_watched:
                name = "load_watched:{}".format(self.request.user.id)
                x = redis.get(name)
                redis.delete(name)
                if x and x == 'true':
                    self.load_watched_task = LoadWatchedTask.delay(self.request.user.id).id

            CustomUser.objects.filter(pk=request.user.pk).update(last_active=timezone.now())

        return super(self.__class__, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super(self.__class__, self).get_context_data(**kwargs)

        if self.request.user.is_authenticated:

            resource = CustomUserResource()
            request_bundle = resource.build_bundle(request=self.request)
            user = resource.obj_get(request_bundle, username=self.request.user.username)
            ur_bundle = resource.build_bundle(obj=user, request=self.request)

            data['currentUser'] = resource.serialize(None, resource.full_dehydrate(ur_bundle), 'application/json')
            data['news'] = json.dumps(get_news_to_display(self.request.user))
            data["offline_mode"] = json.dumps(settings.OFFLINE_MODE)
            data["load_watched_task"] = '"{}"'.format(self.load_watched_task) if self.load_watched_task else "null"
            data["cache_time"] = settings.CACHE_TIME

            if settings.DEBUG:
                data["disable_sending_sentry"] = True

            data["sentry_dsn"] = settings.SENTRY_PUBLIC_DSN
            if "release" in settings.RAVEN_CONFIG:
                data["sentry_version"] = settings.RAVEN_CONFIG["release"]

        else:
            data["stats"] = {
                "channels": cache_count(Channel.objects.all(), "channels"),
                # "users": CustomUser.objects.filter(is_deleted=False).count(),
                "states": cache_count(State.objects.all(), "states"),
                "subscriptions":
                    cache_count(Subscription.objects.filter(
                        deleted=False
                    ), "subscriptions"),
                "videos": cache_count(Video.objects.all(), "videos")
            }
            data["info_email"] = settings.INFO_MAIL

            data["features"] = [
                {
                    "header": "Set states to videos",
                    "description": "There are three states available for you, you can "
                                   "mark videos as watched, ignored and as for later. You never have to see a "
                                   "videos in your feed again if you wish so!",
                    "image": "/static/img/landing/states.png",
                    "large_image": "/static/img/landing/states_large.png",
                },
                {
                    "header": "Choose your favourites",
                    "description": "No time to watch everything? Browse just the channels you are really, "
                                   "really interested in! Keep the rest for some slow Sunday.",
                    "image": "/static/img/landing/favourites.jpg",
                    "large_image": "/static/img/landing/favourites_large.png",
                },
                {
                    "header": "Organise subscriptions into groups",
                    "description": "Create a group for anything you want. Need some music? You can have a group for "
                                   "that. In need of some brain snack? Have a educational group!\n\n"
                                   "Is some group cluttering your feed but you want to stay subscribed? "
                                   "You can exclude the group from the main feed!",
                    "image": "/static/img/landing/groups.png",
                    "large_image": "/static/img/landing/groups_large.png",
                },
                {
                    "header": "Auto ignore some videos",
                    "description": "Dislike a series that a channel does? Well with the Auto Ignore feature you "
                                   "can specify a pattern for a specific channel that will be ignored automatically "
                                   "so it doesn't bother you.",
                    "image": "/static/img/landing/auto_ignore.png",
                    "large_image": "/static/img/landing/auto_ignore_large.png",
                }
            ]

        return data


# 404 NOT FOUND
def error404(request):
    data = client.get_data_from_request(request)
    data.update({
        'level': logging.INFO,
        'logger': 'http404',
    })
    result = client.captureMessage(message='Page Not Found: %s' % request.build_absolute_uri(), data=data)
    if result:
        request.sentry = {
            'project_id': data.get('project', client.remote.project),
            'id': result,
        }
    return render(request, "404.html")  # returns the template


# 500 INTERNAL SERVER ERROR
def error500(request):
    return render(request, "500.html",
                  context={"request": request, "email": settings.INFO_MAIL,
                           "sentry_dns": settings.SENTRY_PUBLIC_DSN}
                  )  # returns the template


class StatisticsView(LoginRequiredMixin, View):
    """ Generates some nice statistics for the users """

    CACHE_TIME = 10 * 60

    def __init__(self, *args, **kwargs):
        super(StatisticsView, self).__init__(*args, **kwargs)
        self.month_ago = None
        self.week_ago = None

    def get_watched_query(self, kwargs=None, user=None):
        queryset = State.objects.filter(watched=True)

        if user is not None:
            queryset = queryset.filter(user=user)

        if kwargs is not None:
            queryset = queryset.filter(**kwargs)

        return queryset

    def get_watched_count(self, kwargs=None, user=None):
        query = self.get_watched_query(kwargs, user)

        return query.count()

    def get_watched_hours(self, kwargs=None, user=None):
        query = self.get_watched_query(kwargs, user).exclude(video__duration_in_seconds=0)

        return (query.aggregate(x=Sum("video__duration_in_seconds"))['x'] or 0.0) / 3600.

    def get_graph_data(self, user=None):
        # type: (Optional[CustomUser]) -> List[Dict[str, Union[str, Optional[int]]]]
        now = timezone.now().date()
        data = []
        queryset = State.objects.filter(watched=True)
        start = now - relativedelta(days=31)

        if user is not None:
            queryset = queryset.filter(user=user)
            start = max(start, user.created.date())

        for dt in date_range_iterator(start, now):  # type: date

            local = queryset.filter(change__day=dt.day,
                                    change__month=dt.month,
                                    change__year=dt.year)

            data.append({
                "date": dt.strftime("%Y-%m-%d"),
                "videos": local.count() or None,
                "hours": ((local.exclude(
                    video__duration_in_seconds=0
                ).aggregate(x=Sum("video__duration_in_seconds"))["x"] or 0.0) / 3600.) or None
            })

        return data

    def get_stats(self, user=None):
        if user is None:
            redis_name = "bettersubbox:stats:system"
        else:
            redis_name = "bettersubbox:stats:{}".format(user.pk)

        if redis.exists(redis_name):
            x = redis.get(redis_name)

            if x is not None:
                return json.loads(x)

        user_kwargs = {}
        videos_kwargs = {}
        if user is not None:
            user_kwargs["user"] = user
            videos_kwargs["channel__subscriptions__user"] = user

        x = {
            "subscriptions": Subscription.objects.filter(**user_kwargs).count(),
            "loaded_videos": Video.objects.filter(**videos_kwargs).count(),
            "watched_total": self.get_watched_count(**user_kwargs),
            "watched_total_time": self.get_watched_hours(**user_kwargs),
            "watched_month": self.get_watched_count(kwargs={"change__gte": self.month_ago}, **user_kwargs),
            "watched_month_time": self.get_watched_hours(kwargs={"change__gte": self.month_ago}, **user_kwargs),
            "watched_week": self.get_watched_count(kwargs={"change__gte": self.week_ago}, **user_kwargs),
            "watched_week_time": self.get_watched_hours(kwargs={"change__gte": self.week_ago}, **user_kwargs),
            "graph": self.get_graph_data(user)
        }

        redis.setex(redis_name, self.CACHE_TIME, json.dumps(x))
        return x

    def get(self, request):
        user = request.user
        now = timezone.now()
        self.week_ago = now - relativedelta(days=7)
        self.month_ago = now - relativedelta(days=31)

        data = {
            "user": self.get_stats(user),
            "system": self.get_stats()
        }

        return JsonResponse(data)


class StaticPage(TemplateView):

    def get_context_data(self, **kwargs):
        data = super(StaticPage, self).get_context_data()

        data["footer_blank"] = True

        return data
