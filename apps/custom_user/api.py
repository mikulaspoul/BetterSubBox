# encoding=utf-8
from tastypie import fields
from tastypie.authentication import SessionAuthentication

from apps.core.api import CustomModelResource, ReadOnlyAuthorization
from apps.custom_user.models import CustomUser


class CustomUserAuthorization(ReadOnlyAuthorization):
    """ User can ready only himself, can update himself as well. """

    def update_list(self, object_list, bundle):
        return object_list

    def update_detail(self, object_list, bundle):
        return True


class CustomUserResource(CustomModelResource):
    """ Api of user. """

    # to make these fields readonly
    first_name = fields.CharField('first_name', readonly=True)
    last_name = fields.CharField('last_name', readonly=True)
    readonly_access = fields.BooleanField('readonly_access', readonly=True)
    older_than = fields.DateField('older_than', null=True, blank=True)

    def get_object_list(self, request):
        return super(CustomUserResource, self).get_object_list(request).filter(pk=request.user.pk)

    class Meta:
        queryset = CustomUser.objects.all()
        resource_name = "custom_user"
        authentication = SessionAuthentication()
        authorization = CustomUserAuthorization()
        always_return_data = True
        list_allowed_methods = ['get', 'post']  # read an update
        detail_allowed_methods = ['get', 'put']  # read an update
        excludes = ['is_active', 'is_deleted', 'is_staff', 'password',
                    'username', 'email', 'watched_playlist_id',
                    'last_watched_id']
