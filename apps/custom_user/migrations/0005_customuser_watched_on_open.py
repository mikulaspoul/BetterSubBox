# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('custom_user', '0004_auto_20141228_2154'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='watched_on_open',
            field=models.BooleanField(default=False, verbose_name='Mark as watched on open'),
            preserve_default=True,
        ),
    ]
