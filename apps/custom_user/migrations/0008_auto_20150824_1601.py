# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def force_resync(apps, schema_editor):
    apps.get_model("custom_user", "CustomUser").objects.all().update(sync_subscriptions=True)


class Migration(migrations.Migration):

    dependencies = [
        ('custom_user', '0007_customuser_older_than'),
    ]

    operations = [
        migrations.RunPython(code=force_resync),
    ]
