# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('custom_user', '0002_auto_20141227_1246'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='player',
            field=models.CharField(default=b'youtube', max_length=20, verbose_name='Player', choices=[(b'youtube', 'YouTube'), (b'full_page', 'Full page')]),
            preserve_default=True,
        ),
    ]
