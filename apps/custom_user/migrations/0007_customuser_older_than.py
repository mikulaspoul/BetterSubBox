# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('custom_user', '0006_auto_20150119_2156'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='older_than',
            field=models.DateField(default=None, null=True, verbose_name='Older than', blank=True),
            preserve_default=True,
        ),
    ]
