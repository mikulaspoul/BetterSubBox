# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('custom_user', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='player',
            field=models.CharField(default='youtube', max_length=20, verbose_name='Player', choices=[(b'youtube', 'YouTube'), (b'embed', 'Embed'), (b'full_page', 'Full page')]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customuser',
            name='sync_subscriptions',
            field=models.BooleanField(default=False, verbose_name='Synchronise subscriptions'),
            preserve_default=True,
        ),
    ]
