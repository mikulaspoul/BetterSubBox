# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('custom_user', '0008_auto_20150824_1601'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='allow_elevator_sounds',
            field=models.BooleanField(default=True, verbose_name='Allow elevator sounds'),
            preserve_default=True,
        ),
    ]
