# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('custom_user', '0005_customuser_watched_on_open'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='sync_subscriptions',
            field=models.BooleanField(default=True, verbose_name='Synchronise subscriptions'),
            preserve_default=True,
        ),
    ]
