# encoding=utf-8
import requests

from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.utils.translation import ugettext_lazy as _

from apps import logger as db_logger


class CustomUserManager(BaseUserManager):
    """
    Mandatory class that handels users.
    """
    def create_user(self, **kwargs):
        user = self.model(**kwargs)
        user.save()
        return user

    def create_superuser(self, **kwargs):
        user = self.model(**kwargs)
        user.save()
        return user


class CustomUser(AbstractBaseUser):
    """
    Customized version of the default django User.
    """

    players = (  # choices of player
        ('youtube', _("YouTube")),
        ('full_page', _("Full page")),
        ('embeded', _("Embeded on page"))
    )

    homepages = (
        ('feed', _("Feed")),
        ('favorite', _("Favorite"))
    )

    layouts = (
        ('regular', _("Regular")),
        ('no-desc', _("No description")),
        ('columns', _("Columns"))
    )

    # info about the user
    username = models.CharField(max_length=50, unique=True, db_index=True, verbose_name=_("User name"))
    first_name = models.CharField(max_length=255, verbose_name=_("First name"))
    last_name = models.CharField(max_length=255, verbose_name=_("Last name"))

    email = models.EmailField(unique=True,
                              max_length=255,
                              verbose_name=_("E-mail"))
    created = models.DateTimeField(auto_now_add=True, verbose_name=_("Created"))
    last_active = models.DateTimeField(verbose_name=_("Last active"), null=True, default=None)
    disconnected = models.DateTimeField(verbose_name=_("Deleted"), null=True, blank=True)

    # settings
    older_than = models.DateField(default=None,
                                  verbose_name=_("Older than"),
                                  blank=True,
                                  null=True)  # used to not display old stuff in Newest

    sync_subscriptions = models.BooleanField(default=True, verbose_name=_("Synchronise subscriptions"))
    player = models.CharField(default="youtube", max_length=20, verbose_name=_("Player"), choices=players)
    watched_on_open = models.BooleanField(default=False, verbose_name=_("Mark as watched on open"))
    allow_elevator_sounds = models.BooleanField(default=False, verbose_name=_("Allow elevator sounds"))
    homepage = models.CharField(default="feed", verbose_name=_("Feed"), choices=homepages, max_length=8)
    layout = models.CharField(default="regular", verbose_name=_("Layout"), choices=layouts, max_length=7)
    confirm_unsubscribe = models.BooleanField(default=True, verbose_name=_("Confirm unsubscribes"))
    first_login = models.BooleanField(default=True, verbose_name=_("First login"))

    load_watched = models.BooleanField(default=False, verbose_name=_("Load watch automatically"))
    watched_playlist_id = models.CharField(max_length=30, verbose_name=_("Watched playlist YouTube ID"), blank=True,
                                           default="")
    last_watched_id = models.CharField(max_length=60, verbose_name=_("Last seen ID"), blank=True, default="")

    readonly_access = models.BooleanField(default=True, verbose_name=_("Readonly access"))

    # the fields below are required by the django auth app
    is_staff = models.BooleanField(default=False,
                                   verbose_name=_('Is staff'))
    is_active = models.BooleanField(default=True,
                                    verbose_name=_('Is active'))

    is_deleted = models.BooleanField(default=False,
                                     verbose_name=_('Is deleted'))  # marks the object as deleted

    is_superuser = models.BooleanField(default=False,
                                       verbose_name=_("Is superuser"))

    objects = CustomUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'email']

    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")

    def save(self, *args, **kwargs):
        """ Save function, modified to download name if needed. """
        super(self.__class__, self).save(*args, **kwargs)

        # when user uses a youtube account that is not linked directly to google(+),
        # the name is not loaded, so this is a fix for that
        if not self.first_name and self.social_auth.count():
            try:  # can't be run on first creation, because social_auth data aren't created yet
                social = self.social_auth.get(provider='google-oauth2')  # users access token
                response = requests.get(
                    'https://www.googleapis.com/youtube/v3/channels',
                    params={'access_token': social.extra_data['access_token'],
                            'mine': 'true',
                            'part': 'snippet'}
                )
                data = response.json()
                self.first_name = data['items'][0]['snippet']['title']

                super(self.__class__, self).save(*args, **kwargs)
            except (KeyError, requests.ConnectionError):
                db_logger.error("YouTube name could not be loaded for user %s" % self)

    def get_full_name(self):
        """ Gets full name of the user. """
        return u"{} {}".format(self.first_name, self.last_name)

    def get_name_or_username(self):
        if not self.first_name and self.last_name:
            return self.username
        else:
            return self.get_full_name()

    get_name_or_username.short_description = _("Name")

    def get_short_name(self):
        """ Gets full name of the user. """
        return u"{}".format(self.first_name)

    def data_ready(self):
        """ Finds outs if the initial data for the user are ready. """

        # are initial data loaded for channels, if not, how many?
        if self.subscriptions.filter(initial=True, channel__initial_data_loaded=False).exists():
            return {"ready": False, "type": "channels",
                    "not_loaded_count": self.subscriptions.filter(initial=True,
                                                                  channel__initial_data_loaded=False).count(),
                    "count": self.subscriptions.filter(initial=True).count()}

        return {"ready": True}

    # django rubish
    @property
    def is_admin(self):
        return self.is_superuser

    def has_perm(self, perm, obj=None):
        return self.is_superuser

    def has_module_perms(self, app_label):
        return self.is_superuser

    def __str__(self):
        return u"User %s: %s" % (self.pk, self.email)
