# encoding=utf-8
from django.contrib import admin
from .models import CustomUser
from django.utils.translation import ugettext_lazy as _


class DisconnectedFilter(admin.SimpleListFilter):

    title = _("Disconnected")

    parameter_name = "disconnected"

    def lookups(self, request, model_admin):
        return (
            ('yes', _("Yes")),
            ('no', _("No"))
        )

    def queryset(self, request, queryset):
        if self.value() == "yes":
            return queryset.filter(disconnected__isnull=False)
        elif self.value() == "no":
            return queryset.filter(disconnected__isnull=True)


class CustomUserAdmin(admin.ModelAdmin):
    list_display = (
        'get_name_or_username', 'email', 'created', 'last_login', 'last_active', 'disconnected'
    )

    list_filter = (
        DisconnectedFilter,
    )


admin.site.register(CustomUser, CustomUserAdmin)
