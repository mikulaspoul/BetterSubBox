# encoding=utf-8
import json

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.views.generic import View
from django.http import HttpResponse, HttpResponseBadRequest
from django.contrib.auth import logout
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from apps import logger


class DataReadView(LoginRequiredMixin, View):
    """ Returns the readiness of initial loading.
    """

    def get(self, request):
        data = request.user.data_ready()
        return HttpResponse(json.dumps(data), content_type="application/json")


class DisconnectCallbackView(LoginRequiredMixin, View):
    """ CallBack for OAuth2 disconnect from youtube
    """

    def dispatch(self, request, *args, **kwargs):

        if request.user.social_auth.count():
            return HttpResponseBadRequest(json.dumps(False), content_type="application/json")

        else:
            request.user.disconnected = timezone.now()
            request.user.save()

            logger.info("%s has disconnected themself from BetterSubBox" % request.user)

            logout(request)
            return HttpResponse(json.dumps(True), content_type="application/json")


class EnableWriteAccessCompleteView(LoginRequiredMixin, View):

    def get(self, request):
        request.user.readonly_access = False
        request.user.save()
        messages.success(request, _("Write access was enabled."))
        return HttpResponseRedirect("/#settings")
