# encoding=utf-8
import logging
from dateutil.relativedelta import relativedelta

from django.conf import settings
from django.utils import timezone

from django_cron import CronJobBase, Schedule

from apps.channel.models import Subscription
from apps.custom_user.models import CustomUser
from apps import client


class DeleteDisconnectedJob(CronJobBase):
    """ Cronjob that deletes all disconnected users, after the period set in settings
    """
    RUN_AT_TIMES = ['3:00']
    RETRY_AFTER_FAILURE_MINS = 5

    schedule = Schedule(run_at_times=RUN_AT_TIMES, retry_after_failure_mins=RETRY_AFTER_FAILURE_MINS)
    code = 'custom_user.delete_disconnected'

    def do(self):
        older_than = timezone.now() - relativedelta(seconds=settings.DISCONNECTED_KEEP_TIME)
        users = CustomUser.objects.filter(disconnected__lt=older_than)
        users2 = "%s" % users

        # the default manager excludes these
        Subscription.all_objects.filter(user__pk__in=users, deleted=True).delete()

        counter = users.count()
        users.delete()
        if counter:
            client.captureMessage("Deleted %s disconnected user(s)" % counter, extra={
                "users": users2
            }, level=logging.INFO)


class DeleteInactiveJob(CronJobBase):
    """ Cronjob that deletes all inactive users, those who haven't logged in for set amount of time
    """
    RUN_AT_TIMES = ['3:05']
    RETRY_AFTER_FAILURE_MINS = 5

    schedule = Schedule(run_at_times=RUN_AT_TIMES, retry_after_failure_mins=RETRY_AFTER_FAILURE_MINS)
    code = 'custom_user.delete_inactive'

    def do(self):
        older_than = timezone.now() - relativedelta(seconds=settings.INACTIVE_KEEP_TIME)
        users = CustomUser.objects.filter(last_login__lt=older_than)
        users2 = "%s" % users
        counter = users.count()

        # the default manager excludes these
        Subscription.all_objects.filter(user__pk__in=users, deleted=True).delete()

        users.delete()
        if counter:
            client.captureMessage("Deleted %s inactive user(s)" % counter, extra={
                "users": users2
            }, level=logging.INFO)
