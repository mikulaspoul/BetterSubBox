# encoding=utf-8
from random import randint

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models.aggregates import Count
from django.conf import settings

from apps.channel.models import Channel

__all__ = ["Video", "State"]


class VideoManager(models.Manager):
    def random(self, user):
        query = self.filter(
            channel__subscriptions__user=user
        ).exclude(
            states__user=user, states__watched=True
        ).exclude(
            states__user=user, states__ignored=True
        ).exclude(
            states__user=user, states__for_later=True
        )

        count = query.aggregate(count=Count('id'))['count']
        random_index = randint(0, count - 1)
        return query[random_index]


class Video(models.Model):
    """ Database representation of video. """

    youtube_id = models.CharField(max_length=50, verbose_name=_("Youtube id"), unique=True, db_index=True)
    channel = models.ForeignKey(Channel, verbose_name=_("Channel"), related_name="videos", on_delete=models.CASCADE)
    title = models.CharField(max_length=255, verbose_name=_("Title"), blank=True, default="")
    duration = models.CharField(max_length=10, verbose_name=_("Duration"), default="", blank=True, null=True)
    duration_in_seconds = models.PositiveIntegerField(verbose_name=_("Duration in seconds"), default=0)

    published = models.DateTimeField(verbose_name=_("published"), blank=True, null=True)

    # the video will be ignored when finding out if the initial videos are loaded
    ignore_initial = models.BooleanField(default=False, verbose_name=_("Ignore initial"))

    objects = VideoManager()

    def save(self, **kwargs):
        self.calculate_duration()
        return super(Video, self).save(**kwargs)

    def calculate_duration(self):
        if self.duration:
            split = self.duration.split(":")
            if len(split) == 2:
                self.duration_in_seconds = int(split[0]) * 60 + int(split[1])
            else:
                self.duration_in_seconds = int(split[0]) * 3600 + int(split[1]) * 60 + int(split[2])

    def __str__(self):
        return "{} - {}".format(self.youtube_id, self.title)

    class Meta:
        verbose_name = _("Video")
        verbose_name_plural = _("Videos")
        get_latest_by = "published"


class State(models.Model):
    """ Database representation of state, how the user marked the video. """

    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("User"), related_name="states",
                             on_delete=models.CASCADE)
    video = models.ForeignKey(Video, verbose_name=_("Video"), related_name="states",
                              on_delete=models.CASCADE)
    watched = models.BooleanField(default=False, verbose_name=_("Watched"))
    ignored = models.BooleanField(default=False, verbose_name=_("Ignored"))
    for_later = models.BooleanField(default=False, verbose_name=_("For later"))
    change = models.DateTimeField(auto_now=True, verbose_name=_("Last change"))

    class Meta:
        verbose_name = _("State")
        verbose_name_plural = _("States")
        unique_together = ("user", "video")  # disallows duplicates

    def to_video_api(self):
        """
        Returns data about the state so it can be added to video api output to limit extra requests.
        """
        return {
            'id': self.pk,
            'watched': self.watched,
            'ignored': self.ignored,
            'for_later': self.for_later,
            'video': self.video.pk,
            'user': self.user_id
        }
