# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('video', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='duration',
            field=models.CharField(default=b'', max_length=10, null=True, verbose_name='Duration', blank=True),
            preserve_default=True,
        ),
    ]
