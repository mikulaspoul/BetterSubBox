# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('channel', '0009_auto_20150824_1410'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('watched', models.BooleanField(default=False, verbose_name='Watched')),
                ('ignored', models.BooleanField(default=False, verbose_name='Ignored')),
                ('for_later', models.BooleanField(default=False, verbose_name='For later')),
                ('change', models.DateTimeField(auto_now=True, verbose_name='Last change')),
                ('user', models.ForeignKey(related_name='states', verbose_name='User', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'State',
                'verbose_name_plural': 'States',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('youtube_id', models.CharField(unique=True, max_length=50, verbose_name='Youtube id')),
                ('title', models.CharField(default=b'', max_length=100, verbose_name='Title', blank=True)),
                ('duration', models.CharField(default=b'', max_length=10, verbose_name='Duration', blank=True)),
                ('published', models.DateTimeField(null=True, verbose_name='published', blank=True)),
                ('ignore_initial', models.BooleanField(default=False, verbose_name='Ignore initial')),
                ('channel', models.ForeignKey(related_name='videos', verbose_name='Channel', to='channel.Channel', on_delete=models.CASCADE)),
            ],
            options={
                'get_latest_by': 'published',
                'verbose_name': 'Video',
                'verbose_name_plural': 'Videos',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='state',
            name='video',
            field=models.ForeignKey(related_name='states', verbose_name='Video', to='video.Video', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='state',
            unique_together=set([('user', 'video')]),
        ),
    ]
