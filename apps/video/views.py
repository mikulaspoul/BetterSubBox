# encoding=utf-8
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.views.generic import TemplateView, View

from apps.video.api import VideoResource
from apps.video.models import Video


class FullPageView(LoginRequiredMixin, TemplateView):
    """ View that displays the video 'fullpage', basically it fills the browser space."""
    template_name = "full_page.html"

    def get_context_data(self, **kwargs):
        data = super(self.__class__, self).get_context_data(**kwargs)
        data['video'] = self.kwargs["video"]
        return data


class RandomVideoView(LoginRequiredMixin, View):
    """ Returns a random video
    """

    def get(self, request):

        video = Video.objects.random(request.user)

        resource = VideoResource()
        ur_bundle = resource.build_bundle(obj=video, request=request)

        return HttpResponse(resource.serialize(None, resource.full_dehydrate(ur_bundle), 'application/json'),
                            content_type="application/json")
