# encoding=utf-8
from datetime import datetime

from dateutil.relativedelta import relativedelta
from django.conf.urls import url
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.db import IntegrityError
from tastypie import fields
from tastypie import http
from tastypie.authentication import SessionAuthentication
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from tastypie.exceptions import Unauthorized
from tastypie.utils import trailing_slash
from tastypie.validation import Validation

from apps.channel.models import Channel, Subscription
from apps.core.api import CustomForeignKey, CustomModelResource, ReadOnlyAuthorization
from apps.group.models import Group
from apps.video.models import Video, State
from apps.youtube.utils import load_extra_data


class VideoResource(CustomModelResource):
    """ Video API """
    # full appends info about channel so no extra request is needed to display name
    channel = CustomForeignKey("apps.channel.api.ChannelResource", "channel", full=True, pk_only=True)
    description = fields.CharField('description', blank=True, null=True)
    thumbnail = fields.CharField('thumbnail', blank=True, null=True)
    quality = fields.CharField('quality', blank=True, null=True)
    captions = fields.BooleanField('captions', blank=True, null=True)

    def get_object_list(self, request):
        return super(VideoResource, self).get_object_list(request).filter(
            channel__subscriptions__user_id=request.user.id,
            channel__subscriptions__deleted=False
        )

    class Meta:
        queryset = Video.objects.select_related("channel").exclude(title="").order_by("-published")  # default ordering
        resource_name = "video"
        authentication = SessionAuthentication()
        authorization = ReadOnlyAuthorization()
        always_return_data = True
        list_allowed_methods = ['get']  # readonly
        detail_allowed_methods = ['get']  # readonly
        excludes = ['initial_data_loaded']
        ordering = ['published']  # allows ordering by published
        filtering = {
            'channel': ALL_WITH_RELATIONS,  # allows filtering via channel
            'published': ALL
        }

    # def dehydrate(self, bundle):
    #     """ Adds info about users state to output, so extra requests are not needed. """
    #
    #     if State.objects.filter(video=bundle.obj, user=bundle.request.user).exists():
    #         bundle.data['state'] = State.objects.get(video=bundle.obj, user=bundle.request.user).to_video_api()
    #     else:
    #         bundle.data['state'] = None
    #
    #     return bundle

    def apply_filters(self, request, applicable_filters):
        """ Applys custom filters. """
        obj_list = super(self.__class__, self).apply_filters(request, applicable_filters)

        # user can set up, that videos older than some date aren't displayed
        newest = request.GET.get('newest', None)
        if newest is not None:
            if request.user.older_than:

                date_filter = request.user.older_than + relativedelta(days=1)  # the day should be displayed
                now = datetime.today().date() + relativedelta(days=1)  # today wouldn't be in the range...

                obj_list = obj_list.filter(published__range=(date_filter, now))

            obj_list = obj_list.exclude(
                channel__in=Channel.objects.filter(
                    pk__in=Group.objects.filter(user=request.user,
                                                exclude_from_feed=True).values("members__channel")
                )
            )

        def user_states(**kwargs):
            return State.objects.filter(user=request.user, **kwargs).values("video")

        # should watched videos be excluded?
        include_watched = request.GET.get('include_watched', None)
        if include_watched == "false":
            obj_list = obj_list.exclude(pk__in=user_states(watched=True))

        # should ignored videos be excluded?
        include_ignored = request.GET.get('include_ignored', None)
        if include_ignored == "false":
            obj_list = obj_list.exclude(pk__in=user_states(ignored=True))

        # should for later videos be excluded?
        include_for_later = request.GET.get('include_for_later', None)
        if include_for_later == "false":
            obj_list = obj_list.exclude(pk__in=user_states(for_later=True))

        # only watched videos are included
        watched = request.GET.get('watched', None)
        if watched:
            obj_list = obj_list.filter(pk__in=user_states(watched=True))

        # only ignored videos are included
        ignored = request.GET.get('ignored', None)
        if ignored:
            obj_list = obj_list.filter(pk__in=user_states(ignored=True))

        # only for later videos are included
        for_later = request.GET.get('for_later', None)
        if for_later:
            obj_list = obj_list.filter(pk__in=user_states(for_later=True))

        # only videos from a group are included
        group = request.GET.get('group', None)
        if group:
            obj_list = obj_list.filter(channel__groups__group__pk=group)

        # fulltext search
        query = request.GET.get('query', None)
        if query:
            obj_list = obj_list.filter(title__icontains=query)

        # a specific date
        date = request.GET.get('date', None)
        if date:
            (year, month, day) = date.split("-")
            obj_list = obj_list.filter(published__year=year,
                                       published__month=month,
                                       published__day=day)

        # only favorite channels
        favorites = request.GET.get('favorites', None)
        if favorites is not None:
            obj_list = obj_list.filter(channel__in=Subscription.objects.filter(favorite=True,
                                                                               user=request.user).values("channel"))

        only_latest_video = request.GET.get("only_latest_video", None)
        if only_latest_video is not None:
            obj_list = Video.objects.filter(
                pk__in=obj_list.distinct("channel").order_by("channel_id", "-published")
            ).order_by("-published")

        # specific ids for cache-style loading
        ids = request.GET.get("ids", None)
        if ids is not None:
            obj_list = obj_list.filter(pk__in=ids.split(","))

        return obj_list.distinct()

    def apply_sorting(self, obj_list, options=None):
        """ Applies custom sorting, specifically by change of state (watched, ignored and for later) """
        if 'order_by' in options:
            if options['order_by'] in ['change', '-change']:
                if options['order_by'] == 'change':
                    obj_list = obj_list.order_by("states__change")
                else:
                    obj_list = obj_list.order_by("-states__change")
            else:
                obj_list = super(self.__class__, self).apply_sorting(obj_list, options)
        else:
            obj_list = super(self.__class__, self).apply_sorting(obj_list, options)
        return obj_list

    def get_detail(self, request, **kwargs):
        basic_bundle = self.build_bundle(request=request)

        try:
            obj = self.cached_obj_get(bundle=basic_bundle, **self.remove_api_resource_names(kwargs))
        except ObjectDoesNotExist:
            return http.HttpNotFound()
        except MultipleObjectsReturned:
            return http.HttpMultipleChoices("More than one resource is found at this URI.")

        extra_data = load_extra_data([obj.youtube_id])

        if obj.youtube_id in extra_data:
            if obj.title != extra_data[obj.youtube_id]["title"] \
                    or obj.duration != extra_data[obj.youtube_id]["duration"]:
                obj.title = extra_data[obj.youtube_id]["title"]
                obj.duration = extra_data[obj.youtube_id]["duration"]
                obj.save()

            for thing in extra_data[obj.youtube_id]:
                setattr(obj, thing, extra_data[obj.youtube_id][thing])
        else:
            obj.delete()
            return http.HttpNotFound()

        bundle = self.build_bundle(obj=obj, request=request)
        bundle = self.full_dehydrate(bundle)
        bundle = self.alter_detail_data_to_serialize(request, bundle)
        return self.create_response(request, bundle)

    def get_list(self, request, **kwargs):
        base_bundle = self.build_bundle(request=request)
        objects = self.obj_get_list(bundle=base_bundle, **self.remove_api_resource_names(kwargs))
        sorted_objects = self.apply_sorting(objects, options=request.GET).select_related("channel")

        paginator = self._meta.paginator_class(request.GET,
                                               sorted_objects,
                                               resource_uri=self.get_resource_uri(),
                                               limit=self._meta.limit,
                                               max_limit=self._meta.max_limit,
                                               collection_name=self._meta.collection_name)
        to_be_serialized = paginator.page()

        ids = [o.youtube_id for o in to_be_serialized[self._meta.collection_name]]

        extra_data = load_extra_data(ids)

        bundles = []

        for obj in to_be_serialized[self._meta.collection_name]:
            if obj.youtube_id in extra_data:
                if obj.title != extra_data[obj.youtube_id]["title"] or \
                   obj.duration != extra_data[obj.youtube_id]["duration"]:
                    obj.title = extra_data[obj.youtube_id]["title"]
                    obj.duration = extra_data[obj.youtube_id]["duration"]
                    obj.save()

                for thing in extra_data[obj.youtube_id]:
                    setattr(obj, thing, extra_data[obj.youtube_id][thing])

                bundle = self.build_bundle(obj=obj, request=request)
                bundles.append(self.full_dehydrate(bundle, for_list=True))
            else:
                obj.delete()

        to_be_serialized[self._meta.collection_name] = bundles
        to_be_serialized = self.alter_list_data_to_serialize(request, to_be_serialized)
        return self.create_response(request, to_be_serialized)

    def prepend_urls(self):
        """ Add the following array of urls to base urls """
        return [
            url(r"^(?P<resource_name>%s)/ids%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('ids')),
        ]

    def ids(self, request, **kwargs):
        """ Returns the list of video ids and any state if one exists
        :param request: request, duh
        :param kwargs: kwargs, duh
        :return: HttpResponse
        """
        base_bundle = self.build_bundle(request=request)
        self.is_authenticated(request)
        objects = self.obj_get_list(bundle=base_bundle)

        sorted_objects = self.apply_sorting(objects, options=request.GET)

        paginator = self._meta.paginator_class(request_data=request.GET,
                                               objects=sorted_objects,
                                               resource_uri=self.get_resource_uri(),
                                               limit=self._meta.limit,
                                               max_limit=self._meta.max_limit,
                                               collection_name=self._meta.collection_name)

        to_be_serialized = paginator.page()

        new_data_dict = {
            "meta": {
                "total_count": to_be_serialized["meta"]["total_count"]
            },
            "objects": []
        }

        queryset = to_be_serialized["objects"].values_list("id", flat=True)

        new_data = []

        append_no_videos_since_last_watched = "only_latest_video" in request.GET

        for video_id in queryset:
            try:
                state = State.objects.get(video_id=video_id, user_id=request.user.id)
            except ObjectDoesNotExist:
                state = None

            video = {"id": video_id, "state": None}

            if state is not None:
                video["state"] = {
                    "id": state.id,
                    "watched": state.watched,
                    "ignored": state.ignored,
                    "for_later": state.for_later,
                    "change": state.change
                }

            if append_no_videos_since_last_watched:
                video["no_videos_since_last_watched"] = Video.objects.select_related("channel").get(
                    pk=video_id
                ).channel.no_videos_since_last_watched(request)

            new_data.append(video)

        new_data_dict["objects"] = new_data

        return self.create_response(request, new_data_dict)


class StateAuthorization(ReadOnlyAuthorization):
    """ User can only ready his states. """

    def create_detail(self, object_list, bundle):
        return True

    def update_list(self, object_list, bundle):
        return object_list

    def update_detail(self, object_list, bundle):
        return True

    def delete_list(self, object_list, bundle):
        return object_list.none()

    def delete_detail(self, object_list, bundle):
        raise Unauthorized("You can't delete states")


class StateValidation(Validation):
    """ Validates users input. """
    def is_valid(self, bundle, request=None):
        errors = {}

        # read error message
        if 'video' not in bundle.data:
            errors['video'] = ["You have set the video."]
        else:
            video = Video.objects.get(pk=bundle.data['video'])
            if not video.channel.subscriptions.filter(user=bundle.request.user).exists():
                errors['video'] = ["You can't create states for video from channel you aren't subscribed to."]

        if 'user' not in bundle.data:
            errors['user'] = ["You have to set user"]
        elif bundle.data['user'] != request.user.pk:
            errors['user'] = ["You can't create/edit states for other users"]

        return errors


class StateResource(CustomModelResource):
    """ State API. """
    video = CustomForeignKey(VideoResource, "video", full=False, pk_only=True)
    user = CustomForeignKey("apps.custom_user.api.CustomUserResource", "user", full=False, pk_only=True)

    def get_object_list(self, request):
        return super(StateResource, self).get_object_list(request).filter(
            video__channel__subscriptions__user_id=request.user.id,
            video__channel__subscriptions__deleted=False,
            user_id=request.user.id)

    def obj_create(self, bundle, **kwargs):
        bundle.obj = self._meta.object_class()

        for key, value in kwargs.items():
            setattr(bundle.obj, key, value)

        bundle = self.full_hydrate(bundle)

        try:
            if State.objects.filter(user_id=bundle.obj.user_id, video_id=bundle.obj.video_id).exists():
                raise IntegrityError
            return self.save(bundle)
        except IntegrityError:
            bundle.obj.id = State.objects.filter(user_id=bundle.obj.user_id, video_id=bundle.obj.video_id)[0].id
            return self.save(bundle)

    class Meta:
        queryset = State.objects.all()
        resource_name = "state"
        authentication = SessionAuthentication()
        authorization = StateAuthorization()
        validation = StateValidation()
        always_return_data = True
        ordering = ['change']  # allows ordering by change

        excludes = ['initial_data_loaded']
