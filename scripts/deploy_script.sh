#!/bin/bash
git checkout master
if git pull; then
  git checkout $1
else
  exit 1
fi
gulp --build=1
python manage.py collectstatic --noinput

if [[ $2 == "prod" ]]; then
  DEPLOY_DIR="/var/www/hosts/bettersubbox.com/www/BetterSubBox/"
else
  DEPLOY_DIR="/var/www/hosts/bettersubbox.com/dev/BetterSubBox/"
fi

cp -R apps collected_static scripts templates wsgi manage.py precise_version requirements.txt settings.py urls.py version $DEPLOY_DIR
