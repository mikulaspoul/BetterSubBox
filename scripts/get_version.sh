#!/bin/bash

BRANCHTAG=`git describe --tags --exact-match 2>/dev/null || git rev-parse --abbrev-ref HEAD`
PRESICE="$BRANCHTAG".`git rev-parse HEAD`
VERSION="$BRANCHTAG".`git rev-list HEAD --count`

echo -n "$VERSION" > version
echo -n "$PRESICE" > precise_version