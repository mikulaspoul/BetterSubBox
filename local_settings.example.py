
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'bettersubbox',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '',
    }
}

# mailcatcher
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = '127.0.0.1'
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_PORT = 1025
EMAIL_USE_TLS = False

REDIS_PASSWORD = ""

RAVEN_CONFIG = {
    'dns': None
}

try:
    with open("precise_version") as file:
        release = file.read()

    RAVEN_CONFIG["release"] = release
except BaseException:
    pass

SENTRY_PUBLIC_DSN = None

# GOOGLE AUTH SETTINGS
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = None
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = None

# YOUTUBE SETTINGS
YOUTUBE_API_KEY = None

DEBUG = True

SECRET_KEY = None

if DEBUG:
    RAVEN_CONFIG["dns"] = None
