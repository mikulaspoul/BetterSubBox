"""
WSGI config for bettersubbox project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os
import sys

sys.path.append("/var/www/hosts/bettersubbox.com/www/BetterSubBox/")

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

from django.core.wsgi import get_wsgi_application  # noqa
from raven import Client  # noqa
from raven.middleware import Sentry  # noqa

application = get_wsgi_application()

from django.conf import settings  # noqa

application = Sentry(
    application,
    Client(settings.RAVEN_CONFIG["dsn"])
)
