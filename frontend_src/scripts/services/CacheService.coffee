'use strict'

"""
Services keep their data and functions even when controllers change, but even though the data is relevant only to
one view, I decided to use them for the functionality of loading videos. Otherwise, the functionality would had to be copied in every controller, since javascript inheritance sucks.
"""


angular.module('bettersubboxApp.CacheService', [])
.factory "CacheService", (localStorageService, APIService, Video, $rootScope ) ->
  CacheService = {}

  lsKeys = localStorageService.keys()

  now = moment().format("X")
  valid_till = moment().add(window.cache_time, "s").format("X")

  angular.forEach(lsKeys, (key) ->
    if key.indexOf("video_") > -1
      video = localStorageService.get(key)
      if not video._valid? or video._valid < now
        localStorageService.remove(key)
  )

  CacheService.lsName = (id) ->
    return "video_"+id

  CacheService.getVideo = (id) ->
    return localStorageService.get(CacheService.lsName(id))

  CacheService.setVideo = (id, value) ->
    value._valid = valid_till
    localStorageService.set(CacheService.lsName(id), value)

  CacheService.get = (query, success, error) ->
    query.videoId = "ids"
    Video.get query, (data) ->
      videos = {}
      states = {}

#      console.log([x.id for x in data.objects][0])

      idsToLoad = []
      angular.forEach data.objects, (obj) ->
        states[obj.id] = obj.state

        value =  CacheService.getVideo(obj.id)
        if not value
          idsToLoad.push obj.id
        else
          videos[obj.id] = value

      process = ->
        localData = {}
        localData.meta = data.meta
        localData.objects = []
        angular.forEach data.objects, (obj) ->
          if videos[obj.id]
            x = videos[obj.id]
            x.state = states[obj.id]

            if obj.no_videos_since_last_watched? isnt null
              x.no_videos_since_last_watched = obj.no_videos_since_last_watched

            localData.objects.push(x)

        if success?
#          console.log([x.id for x in localData.objects][0])
          success(localData)

      if idsToLoad.length
        Video.query {"ids": idsToLoad.join(","), limit: 1000}, (data) ->
          angular.forEach data.objects, (obj) ->
            CacheService.setVideo(obj.id, obj)
            videos[obj.id] = obj

          process()

        , (response) ->
          if error?
            error(response)
      else
        process()


    , (response) ->
      if error?
        error(response)

  return CacheService
