'use strict'

""" A simple service to share the searchQuery between the two seperate inputs and to share the serch function
"""

angular.module('bettersubboxApp.SearchService', [])
.factory 'SearchService', ($state, SelectedFilters, VideoService) ->
    # which filters are selected, what is supposed to be included
    SearchService = {}

    SearchService.searchQuery = ""
    SearchService.search = (searchQuery) ->
        SelectedFilters.searchQuery = searchQuery

        # you cant go to a state you are already on, so just reloads the parameter -> VideoService applies the change
        if $state.current.name == "videos.search"
            VideoService.set("query", searchQuery)
        else # goes to the search, query is stored in SelectedFilters
            $state.go("videos.search")

    return SearchService

