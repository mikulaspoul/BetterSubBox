'use strict'

"""
Services keep their data and functions even when controllers change, but even though the
data is relevant only to one view,  I decided to use them for the functionality of loading
videos. Otherwise, the functionality would had to be copied in every controller, since
javascript inheritance sucks.
"""

angular.module('bettersubboxApp.VideoService', [])
.factory 'SelectedFilters', () ->
    # which filters are selected, what is supposed to be included
    SelectedFilters = {}

    SelectedFilters.includeWatched = false
    SelectedFilters.includeIgnored = false
    SelectedFilters.includeForLater = false
    SelectedFilters.searchQuery = ""

    return SelectedFilters

.factory 'VideoService', (SelectedFilters, Video, $timeout, State, growl, User, APIService, $rootScope,
                          CacheService, $http, $anchorScroll, $window ) ->

    VideoService = {} # object
    VideoService.type = "newest" # current type, default is newest
    VideoService.parameter = true # current parametr, default is true for newest
    VideoService.limit = 12
    VideoService.query = {
        limit: VideoService.limit
    } # what query is sent to server
    VideoService.videos = [] # loaded videos
    VideoService.user = window.user
    VideoService.layout = VideoService.user.layout

    VideoService.setLayout = (layout) ->
      VideoService.layout = layout
      $rootScope.$broadcast "layout-change"
      VideoService.initReadmore()

    # displays loading message, empty message and so on
    VideoService.loading = false
    VideoService.empty = false
    VideoService.totalCount = 0
    VideoService.visible = 0
    VideoService.embedThis = null
    VideoService.get_running = false
    VideoService.disable_infinite = false
    VideoService.last_updated_video = null

    # gets videos
    VideoService.get = (callback) ->
        if VideoService.get_running
            return

        VideoService.disable_infinite = true
        VideoService.get_running = true
        VideoService.loading = true # loading message is displayed

        # what is the offset
        # this method is not at all ideal, it fails when the user does something with states in a different instance
        VideoService.recountVisible()
        VideoService.query.offset = VideoService.visible
        VideoService.query.limit = if VideoService.visible then VideoService.limit / 2 else VideoService.limit

        if VideoService.videos.length > 0
            VideoService.query.published__lte = VideoService.videos[0].published

        CacheService.get VideoService.query, (data) ->
            if data.objects.length == 0 # no more videos are available
                VideoService.empty = true
                VideoService.recountVisible()
                VideoService.loading = false
                VideoService.get_running = false
                return
            else
                VideoService.empty = false

            VideoService.totalCount = data.meta.total_count # updates total count, how many videos could be loaded

            angular.forEach data.objects, (video) -> # adds to video list
                if VideoService.videos.filter((vid) -> video.id == vid.id).length == 0
                    VideoService.videos.push video
                else if window.debug
                    growl.error "MULTIPLE IDS IN THE THINGS!" ,
                                {ttl: 100000, title: "MULTIPLE IDS" }
                    console.log "multiple ids"


            # there are no videos loaded, list is empty
            if VideoService.videos.length == 0
                VideoService.empty = true
            else
                VideoService.empty = false

            VideoService.recountVisible()
            VideoService.loading = false # loading message is not displayed anymore
            VideoService.initReadmore()
            VideoService.get_running = false

            $timeout ->
                VideoService.disable_infinite = false
                callback?()
            , 1000

        , (response) -> # error
            APIService.treatError(response)
            VideoService.get_running = false

    # reloads videos
    VideoService.reload = () ->
        VideoService.set(VideoService.type, VideoService.parameter)

    # sets which videos should be loaded
    # the type and parameter is saved, query is created
    # it is determined if includes are supposed to be added ( include Watched and so on)
    VideoService.set = (type, parameter) ->
        $window.scrollTo(0, 0)

        VideoService.query = {
            limit: VideoService.limit
        }

        if type == "newest"
            includes = true
            VideoService.type = "newest"
            VideoService.parameter = "newest"
            VideoService.query.newest = true
        else if type == "group"
            includes = true
            VideoService.type = "group"
            VideoService.parameter = parameter
            VideoService.query.group = parameter
        else if type == "channel"
            includes = true
            VideoService.type = "channel"
            VideoService.parameter = parameter
            VideoService.query.channel = parameter
        else if type == "favorites"
            includes = true
            VideoService.type = "favorites"
            VideoService.parameter = "favorites"
            VideoService.query.favorites = true
        else if type == "only_latest_video"
            includes = true
            VideoService.type = "only_latest_video"
            VideoService.parameter = "only_latest_video"
            VideoService.query.only_latest_video = true
        else if type == "for_later"
            includes = false
            VideoService.type = "for_later"
            VideoService.parameter = "for_later"
            VideoService.query.for_later = true
            VideoService.query.order_by = "-change"
        else if type == "watched"
            includes = false
            VideoService.type = "watched"
            VideoService.parameter = "watched"
            VideoService.query.watched = true
            VideoService.query.order_by = "-change"
        else if type == "ignored"
            includes = false
            VideoService.type = "ignored"
            VideoService.parameter = "ignored"
            VideoService.query.ignored = true
            VideoService.query.order_by = "-change"
        else if type == "query"
            includes = true
            VideoService.type = "query"
            VideoService.parameter = parameter
            VideoService.query.query = parameter
        else if type == "date"
            includes = true
            VideoService.type = "date"
            VideoService.parameter = parameter
            VideoService.query.date = parameter
            VideoService.query.order_by = "published"

        if includes
            VideoService.query.include_watched = SelectedFilters.includeWatched
            VideoService.query.include_ignored = SelectedFilters.includeIgnored
            VideoService.query.include_for_later = SelectedFilters.includeForLater

        VideoService.clear() # removes currently loaded videos)
        VideoService.loading = false
        VideoService.get() # loads videos

    VideoService.clear = () ->
        VideoService.videos = []

    # updates state
    VideoService.setState = (video, type, value, from_sidebar=false) ->
        reload_if_latest = ->
            if VideoService.type == "only_latest_video"
                VideoService.reload()

        if video.state # state already exists, updated is needed
            video.state.watched = false
            video.state.ignored = false
            video.state.for_later = false

            video.state.user = VideoService.user.id
            video.state.video = video.id

            video.state[type] = value

            State.update video.state, () ->
                growl.success "Status updated.", {title: "Success", ttl: 5000}
                reload_if_latest()
            , (response) ->
                growl.error "There was a unexpected error. Try again later, please.", {title: "Error", ttl: 5000}

        else # new state needs to be created
            state = new State()
            state[type] = value
            state.user = VideoService.user.id
            state.video = video.id

            State.save state, (createdState) ->
                video.state = createdState
                growl.success "Status updated.", {title: "Success", ttl: 5000}
                reload_if_latest()
            , (response) ->
                growl.error "There was a unexpected error. Try again later, please.", {title: "Error", ttl: 5000}

        if from_sidebar
            for from_main in VideoService.videos
                if from_main.id == video.id
                    from_main.state = video.state

            VideoService.initReadmore()

        VideoService.last_updated_video = video

        # how many videos are visible -> it is possible no more are visible -> message
        VideoService.recountVisible()

    VideoService.recountVisible = () ->
        oldVisible = VideoService.visible
        VideoService.visible = 0
        angular.forEach VideoService.videos, (video) ->
            if VideoService.showVideo(video)
                VideoService.visible += 1

    # should the video be showed, if not, set visible to false just in case user set up embedding and opened the video
    VideoService.showVideo = (video) ->
        rtrn = null
        if VideoService.type == "watched"
            if video.state and video.state.watched == false
                rtrn = false
            rtrn = true
        if VideoService.type == "ignored"
            if video.state and video.state.ignored == false
                rtrn = false
            rtrn = true
        if VideoService.type == "for_later"
            if video.state and video.state.for_later == false
                rtrn = false
            rtrn = true

        if rtrn == null
            if video.state
                rtrn = true
                if video.state.watched == true and SelectedFilters.includeWatched == false
                    rtrn = false
                if video.state.ignored == true and SelectedFilters.includeIgnored == false
                    rtrn = false
                if video.state.for_later == true and SelectedFilters.includeForLater == false
                    rtrn = false
            else
                rtrn = true
        if not rtrn
          video.visible = false
        return rtrn

    # thumbnail is displayed is done directly, but error is thrown into the console
    VideoService.channelThumbnail = (video) ->
        return video.channel.thumbnail

    # thumbnail is displayed is done directly, but error is thrown into the console
    VideoService.videoThumbnail = (video) ->
        return video.thumbnail

    # include filters don't make sense for some views
    VideoService.displayIncludeFilters = (show) ->
        if !show
            return $(".includeFilters").slideUp()
        else
            return $(".includeFilters").slideDown()

    # can the load more button be displayed
    VideoService.displayLoadMoreButton = () ->
        if VideoService.empty == true # initial load returned nothing
            return false
        else if VideoService.loading == true # currently loading videos
            return false
        else if VideoService.totalCount == VideoService.videos.length # total count is the same as videos length
            return false
        else
            return true

    VideoService.displayEmpty = () ->
        return VideoService.empty

    # launched on every open of video, user can set up that the videos are marked as watched by default
    VideoService.markAsWatched = (video) ->
        if VideoService.user.watched_on_open == true and (video.state == null or video.state.watched == false)
            VideoService.setState(video, "watched", true)

    VideoService.initReadmore = () ->
        $timeout ->
            $(".description").readmore({
                beforeToggle: (trigger, element, expanded) ->
                    if expanded
                        element2 = $("#video-"+$(element).attr("youtube-id"))
                        $('html, body').animate( { scrollTop: element2.offset().top }, {duration: 200 } );
            })

    VideoService.check_watched = ->
        $http({
            method: "GET",
            url: "/load_watched/result/",
            params: {
                task_id: window.load_watched_task
            }
        }).then( (response) ->
            data = response.data
            if not data.ready
                setTimeout(VideoService.check_watched, 5000)
            else if data.failed
                opts = {title: "Load watched videos task failed", ttl: 5000}

                if data.status == "expired_code"
                    growl.error "Load watched videos task failed because the current login session is too old. Logout and login again.", opts
                else
                    growl.error "Load watched videos task failed. Please try again later", opts

            else if data.data
                angular.forEach(data.data, (video_id) ->
                    state = new State()
                    state.watched = true
                    state.user = VideoService.user.id
                    state.video = video_id
                    angular.forEach(VideoService.videos, (video) ->
                        if video.id == video_id
                            video.state = state
                    )
                )
                growl.success "Load watched videos task finished and marked the videos",
                    {title: "Load watched videos task finished", ttl: 15000}
        )

    if window.load_watched_task
        setTimeout(VideoService.check_watched, 5000)


    return VideoService
