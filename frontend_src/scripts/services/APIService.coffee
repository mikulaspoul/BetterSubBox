'use strict'

"""
Resources, links to API... Angular takes care of it.
"""

angular.module('bettersubboxApp.APIService', [])
.factory 'APIService', ($state, growl) ->
  return {
    treatError: (response, modal = false) ->
      if not response
        return

      if response.status == 503
        if window.offline_mode
          return
        else
          window.location.href = "/youtube_down"
          return

      if not response.data
        response.data = {}

      if response.error and not response.data.error
        response.data.error = response.error

      if response.data.error
        if response.data.error instanceof Array
          for error in response.data.error
            str = error.replace /\[u\'/, ""
            str = str.replace /\'\]/, ""
            growl.error str, {ttl: 10000, title: "Error"  }
        else
          str = response.data.error.replace /\[u\'/, ""
          str = str.replace /\'\]/, ""
          growl.error str, {ttl: 10000, title: "Error"  }

      if response.status >= 500
        if response.data.error_message
          growl.error response.data.error_message, {ttl: 10000, title: "Error"  }
        else
          growl.error "Unknown server error has occured. Please try again later." , {ttl: 10000, title: "Internal Server Error" }

      else if response.status == 401
        growl.error "You are not allowed to do that." , {ttl: 10000, title: "Unauthorized"   }
        $state.go "401"

      else if Object.keys(response.data).length > 0
        if (Object.keys(response.data).length == 1 and not response.data.error) or Object.keys(response.data).length > 1
          for key in Object.keys(response.data)
            if response.data[key].__all__
              growl.error response.data[key].__all__, {ttl: 10000, title: "Error" }
            else
              growl.error "There is an error in at least one of the fields."  , {ttl: 10000, title: "Field error" }

      #console.log response
      if not modal
        if response.status == 500
          $state.go "500"
  }

.factory 'User', ($resource) ->
  return $resource 'api/v1/custom_user/:userId/', {userId: '@id'}, {
    response_name: "custom_user"
    query: {method:'GET', params:{}}
    update: {method:'PUT'}
  }

.factory 'Channel', ($resource) ->
  return $resource 'api/v1/channel/:channelId/', {channelId: '@id'}, {
    response_name: "channel"
    query: {method:'GET', params:{}}
  }

.factory 'Subscription', ($resource, growl) ->
  x = $resource 'api/v1/subscription/:subscriptionId/', {subscriptionId: '@id'}, {
    response_name: "subscription"
    query: {method:'GET', params:{}}
    update: {method:'PUT'}
    save: {method:'POST'}
    remove: {method:'DELETE'}
  }

  x.unsubscribe = (subscription_id, callback) ->
    x.remove {subscriptionId: subscription_id}, () ->
      growl.success "Unsubscribed.", {title: "Success", ttl: 5000}
      callback?()
    , (response) ->

      if response.data.failed and response.data.reason == "invalid_credentials"
        url = window.credentials_refresh_with_unsubscribe.replace("PK", ""+subscription_id)

        growl.error "The login credentials we had from YouTube expired, we can't unsubscribe you from " +
                    "channels until you logout and log back in." +
                    "<div class='text-center'><a class='btn btn-primary' href='"+ url + "'>Click here to refresh credentials</a></div>", {
          title: "Error",
          ttl: -1,
        }
      else if response.data.failed and response.data.reason == "insufficient_permission"
        growl.error "We only have readonly permissions for your profile. If you wish to unsubscribe from " +
                    "channels you have to enable write access in settings." +
                    "<div class='text-center'><br><a href='/#/settings' class='btn btn-primary'>Click here to go to settings</a></div>", {
          title: "Error",
          ttl: -1,
        }
      else
        growl.error "There was a unexpected error. You may have already unsubscribed, " +
                    "try to rerun the subscriptions sync.", {title: "Error", ttl: -1}

  return x

.factory 'AutoIgnore', ($resource) ->
  return $resource 'api/v1/auto_ignore/:autoIgnoreId/', {autoIgnoreId: '@id'}, {
    response_name: "auto_ignore"
    query: {method:'GET', params:{}}
    save: {method:'POST'}
    remove: {method:'DELETE'}
  }

.factory 'Group', ($resource) ->
  return $resource 'api/v1/group/:groupId/', {groupId: '@id'}, {
    response_name: "group"
    query: {method:'GET', params:{}}
    update: {method:'PUT'}
    save: {method:'POST'}
    remove: {method:'DELETE'}
  }

.factory 'GroupMembership', ($resource) ->
  return $resource 'api/v1/group_membership/:groupMembershipId/', {groupMembershipId: '@id'}, {
    response_name: "group_membership"
    query: {method:'GET', params:{}}
    update: {method:'PUT'}
    save: {method:'POST'}
    remove: {method:'DELETE'}
  }

.factory 'Video', ($resource) ->
  return $resource 'api/v1/video/:videoId/', {videoId: '@id'}, {
    response_name: "video"
    query: {method:'GET', params:{}}
  }

.factory 'State', ($resource) ->
  return $resource 'api/v1/state/:stateId/', {stateId: '@id'}, {
    response_name: "state"
    query: {method:'GET', params:{}}
    update: {method:'PUT'}
    save: {method:'POST'}
  }
