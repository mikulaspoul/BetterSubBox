'use strict'

filters = angular.module('bettersubboxApp.filters')

filters.filter 'moment', () ->
  # filter for the momentJS lib
  # usage: date | moment:'M/D/YYYY'
  return (dateString, format) ->
    return moment(dateString).format(format)
filters.filter 'mdate', () ->
  # shortcut for moment date filter
  return (dateString) ->
    return moment(dateString).format "D. M. YYYY"
filters.filter 'mtime', () ->
  # shortcut for moment time filter
  return (dateString) ->
    return moment(dateString).format "H:mm"
filters.filter 'datetime', () ->
  # shortcut for moment datetime filter
  return (dateString) ->
    return moment(dateString).format "D. M. YYYY H:mm"

# replaces url strings with links
filters.filter 'parseUrl', ->
  replacePattern = /\b((http:\/\/|https:\/\/|ftp:\/\/|mailto:|news:)|www\.|ftp\.|[^ \,\;\:\!\)\(\""\'\<\>\f\n\r\t\v]+@)([^ \,\;\:\!\)\(\""\'\<\>\f\n\r\t\v]+)\b/g
  (text, target, otherProp) ->
    originText = text
    if not text? or text is ""
      ""
    else
      text.replace replacePattern, ($0, $1) ->
        match = $0
        protocol = $1
        return "<a target=\"_blank\" href=\"http://" + match + "\">" + match + "</a>"  if (/^www\./i).test(match)
        return "<a target=\"_blank\" href=\"ftp://" + match + "\">" + match + "</a>"  if (/^ftp\./i).test(match)
        return "<a target=\"_blank\" href=\"mailto:" + match + "\">" + match + "</a>"  if protocol and protocol.charAt(0) is "@"
        "<a target=\"_blank\" href=\"" + match + "\">" + match + "</a>"

# marks html as safe, so it's displayed
filters.filter "toTrusted", [
  "$sce"
  ($sce) ->
    return (text) ->
      $sce.trustAsHtml text
]

filters.filter 'range', ->
    (n) ->
      res = []
      i = 0
      while i < n
        res.push i
        i++
      res

filters.filter 'slice', ->
  (list, offset, limit) ->
    if list
      list.slice(offset, offset+limit)
    else
      return []

filters.filter 'visible', (VideoService ) ->
  (list) ->
    rtrn = list.filter((video) ->
      VideoService.showVideo(video)
    )
    return rtrn

filters.filter 'displayDecimal', ->
  (number, numberOfDecimals=2) ->
    return Math.round(parseFloat(number) * Math.pow(10, numberOfDecimals)) / Math.pow(10, numberOfDecimals)
