'use strict'

angular.module('bettersubboxApp.controllers')
  .controller 'MainCtrl', ($scope, $uibModal, $http) ->
    # main ctrl, nothing is needed here, it just needs to be defined
    news = window.news

    $scope.$watch "showSearch", ->
      if $scope.showSearch
        $(".navbar-show-search").removeClass("closed")
        $(".navbar-search").slideDown()
        $(".navbar-search input").focus()

      else
        $(".navbar-show-search").addClass("closed")
        $(".navbar-search").slideUp()

    setTimeout ->

      $(".sidebar-nav a").on "click", ->
        x = $(this)
        if x.attr("href") and x.closest(".sidebar-nav").hasClass("collapse")
          $(".navbar-toggle").trigger("click")


    $scope.readNews = () ->
      if news.length
        ids = []
        angular.forEach (news), (news2) ->
          ids.push news2.id
        $http({
          method: "POST",
          url: "/news/read/",
          data: {
            news: ids
          }
        })

    if news.length
      $uibModalInstance = $uibModal.open {
        templateUrl: '/static/views/news.html'
        controller: ($uibModalInstance, news, $scope) ->
          $scope.news = news
          $scope.dismiss = () -> # closes the window
            $uibModalInstance.dismiss()
        size: 'md'
        resolve: {
          news: () ->
            news
        }
      }

      # realoads groups, because some could be created, deleted
      $uibModalInstance.result.then (newGroup) ->
        $scope.readNews()
      , () ->
        $scope.readNews()

    $scope.noFlexWrap = Modernizr.flexbox and Modernizr.flexwrap


  .controller "HomeCtrl", ($scope, $state) ->
    if $scope.user.homepage == "favorite"
      $state.go("videos.favorites")
    else
      $state.go("feed")

  .controller "SearchCtrl", ($scope, SearchService) ->
    $scope.SearchService = SearchService

  .controller 'SidebarCtrl', ($scope, SelectedFilters, Group, Channel, $uibModal, VideoService, $state, APIService,
                              SearchService) ->
    """
    Ctrl that controls the sidebar - changes the include options, loads groups, filters channels.
    """

    # initial data
    $scope.includeWatched = SelectedFilters.includeWatched
    $scope.includeIgnored = SelectedFilters.includeIgnored
    $scope.includeForLater = SelectedFilters.includeForLater
    $scope.channelQuery = ""
    $scope.searchQuery = SelectedFilters.searchQuery

    # VideoService
    $scope.VideoService = VideoService


    # updated include options
    $scope.update = (type, value) ->
      if type == "includeWatched"
        SelectedFilters.includeWatched = value
      if type == "includeIgnored"
        SelectedFilters.includeIgnored = value
      if type == "includeForLater"
        SelectedFilters.includeForLater = value
      VideoService.set(VideoService.type, VideoService.parameter)

    # reloads groups to menu
    $scope.loadGroups = () ->
      Group.get {}, (data) ->
        $scope.groups = data.objects
      , (response) ->
        APIService.treatError(response)

    $scope.loadGroups() # initial load

    $scope.$on "groups-changed-2", ->
      $scope.loadGroups()

    # filters channels based on query
    $scope.filterChannels = (channelQuery) ->
      if channelQuery
        Channel.query {
          title__icontains: channelQuery
          limit: 5
        }, (data) ->
          $scope.channels = data.objects
        , (response) ->
          APIService.treatError(response)
      else
        $scope.channels = []

    # opens manage group
    $scope.manageGroups = () ->
      $uibModalInstance = $uibModal.open {
        templateUrl: '/static/views/manage_groups.html'
        controller: 'ManageGroupsCtrl'
        size: 'md'
      }

      # realoads groups, because some could be created, deleted
      $uibModalInstance.result.then (newGroup) ->
        $scope.loadGroups()
      , () ->
        $scope.loadGroups()

    $scope.SearchService = SearchService

    $scope.date = moment() # current date

    $scope.updateDate = (date) -> # changes date based on users actions
      $scope.date = date
      $scope.dateDisplay = moment(date).format("D. M. YYYY") # what should be displayed

    $scope.searchByDate = () ->
      toState = moment($scope.date).format("YYYY-MM-DD")
      toState2 = toState.split "-"
      $scope.dateDisplay = "" # undisplays
      $state.go "videos.date", {year: toState2[0], month: toState2[1], day: toState2[2]}

    initialized = false

    initMenu = ->
      if not initialized
        if $("#side-menu").size()
          $("#side-menu").metisMenu()
        else
          setTimeout(initMenu, 20)

    initMenu()

    setTimeout ->
      topOffset = 50;
      width = if ($(window).innerWidth() > 0) then $(window).innerWidth() else screen.width;
      if (width < 768)
        $('div.navbar-collapse').addClass('collapse')
        topOffset = 100; # 2-row-menu
      else
        $('div.navbar-collapse').removeClass('collapse')

      height = if ($(window).innerHeight() > 0) then  $(window).innerHeight() else screen.height;
      height = height - topOffset;
      if (height < 1)
        height = 1
      if (height > topOffset)
        $("#page-wrapper").css("min-height", (height) + "px")

      $(".channels-filter").hover(->
        $(".channels-menu").addClass("fake-hover")
      , ->
        $(".channels-menu").removeClass("fake-hover")
      )

    , 20


  .controller 'TopbarCtrl', ($scope) ->
    null

  .controller 'StatisticsCtrl', ($scope, $http, displayDecimalFilter) -> # loads data for display

    $scope.mode = "user"

    is_float = (n) ->
      return Number(n) == n && n % 1 != 0;

    $scope.stats = null
    $scope.line = new Morris.Line({
      element: "stats-chart",
      data: [
      ],
      xkey: "date",
      ykeys: ['hours', 'videos'],
      xLabels: "day",
      xLabelFormat: (x) ->
        return moment(x).format("MMM Do")
      yLabelFormat: (x) ->
        if not x
          return "-"
        else if is_float(x)
          return displayDecimalFilter(x)
        return x
      dateFormat: (x) ->
        return moment(x).format("MMMM Do YYYY")

      labels: ['Hours', 'Videos'],
      lineColors: ["#428bca", "#5cb85c"],
      xLabelAngle: 60,
      hideHover: 'auto'
    })

    $scope.set_mode = (mode) ->
      $scope.mode = mode
      if mode == "user"
        $scope.stats = $scope.all_statistics.user
      else
        $scope.stats = $scope.all_statistics.system

      $scope.draw_chart()

    $http.get('/statistics/')
      .success (data) ->
        $scope.all_statistics = data
        $scope.set_mode("user")

    $scope.draw_chart = ->
      $scope.line.setData($scope.stats.graph)

  .controller "ErrorCtrl", ($scope, $http, APIService ) ->
    $http({
      method: "GET",
      url: "/video/random/"
    }).then((response) ->
      $scope.video = response.data
    , (reponse) ->
      APIService.treatError(response)
    )







