'use strict'

angular.module('bettersubboxApp.controllers')

  .controller 'SettingsCtrl', ($scope, User, growl, VideoService, $http, APIService ) -> # saves settings
    $scope.user = User.get {userId: $scope.user.id}, (data) ->
      null
    , (response) ->
      APIService.treatError(response)

    $scope.players = [ # passible players
      {value: "youtube", name: "YouTube"},
      {value: "full_page", name: "Full page"},
      {value: "embeded", name: "Embeded on page"}
    ]

    $scope.homepages = [ # passible players
      {value: "feed", name: "Feed"},
      {value: "favorite", name: "Favorite channels"}
    ]

    $scope.removeOlderThanDate = () ->
      $scope.user.older_than = null

    $scope.save = () -> # saves user
      # timezones are a bitch
      if $scope.user.older_than
        $scope.user.older_than = moment($scope.user.older_than).format("YYYY-MM-DD")

      User.update $scope.user, (data) ->
        VideoService.user = data
        $scope.$emit "user-updated"
        growl.success "Your settings were updated.", {title: "Success", ttl: 5000}
      , (response) ->
        APIService.treatError(response)

    $scope.deleteAccount = () ->
      sure = confirm("Are you really sure?")

      if not sure
        return

      $http({
        method: "POST",
        url: "/disconnect/google-oauth2/",
      }).then((response) ->
        if response.data
          window.location.href = "/disconnected/"
        else
          growl.error "There was a problem. Try to logout and login and disconnecting again.", {title: "Error", ttl: 10000}
      , (response) ->
        APIService.treatError(response)
      )








