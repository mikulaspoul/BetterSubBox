'use strict'
"""
Controllers of videos lists, but VideoService takes care of everything.
These controllers just set the types of views, header, error message.
"""

angular.module('bettersubboxApp.controllers')
  .controller 'FeedVideoCtrl', ($scope, SelectedFilters, VideoService ) ->
    $scope.VideoService = VideoService
    $scope.VideoService.clear()
    $scope.VideoService.set("newest", true)
    $scope.header = "Feed"
    $scope.noMoreMessage = "There aren't any more videos with current filters."
    $scope.emptyMessage = "It appears you have no subscriptions. Try going to the settings and resyncing subscriptions."

  .controller 'GroupsVideosCtrl', ($scope, SelectedFilters, VideoService, $stateParams, Group, APIService ) ->
    $scope.VideoService = VideoService
    $scope.VideoService.clear()
    $scope.id = $stateParams.slug.split("-")[0]

    $scope.group = Group.get {groupId: $scope.id}, (data) ->
      if $stateParams.slug != $scope.group.slug
        $state.go "videos.group", {slug: $scope.group.slug}
      else
        $scope.VideoService.set("group", $scope.id)
        $scope.header = $scope.group.name
        $scope.noMoreMessage = "There aren't any more videos in this group with current filters."
        $scope.emptyMessage = "Either there are no more videos in this group with current filters or there are no channels in this group"
    , (response) ->
      APIService.treatError(response)

  .controller 'ChannelsVideosCtrl', ($scope, SelectedFilters, VideoService, $stateParams, Channel, Subscription,
                                     $state, growl, APIService, $timeout ) ->
    $scope.VideoService = VideoService
    $scope.VideoService.clear()
    $scope.id = $stateParams.slug.split("-")[0]

    $scope.channel = Channel.get {channelId: $scope.id, include_subscription: true}, (data) ->
      if $stateParams.slug != $scope.channel.slug
        $state.go "videos.channel", {slug: $scope.channel.slug}
        return
      else
        $scope.VideoService.set("channel", $scope.channel.id)

        $scope.header = $scope.channel.title
        $scope.noMoreMessage = "There aren't any more videos for this channel with current filters."
        $scope.emptyMessage = "Either no videos match the current filters or the channel doesn't have any videos."
    , (response) ->
      APIService.treatError(response)

    $scope.favorite_change = () ->
      $scope.channel.subscription.favorite = !$scope.channel.subscription.favorite
      Subscription.update $scope.channel.subscription, () ->
        if $scope.channel.subscription.favorite
          growl.success "Channel favorited", {title: "Success", ttl: 5000}
        else
          growl.success "Channel unfavorited", {title: "Success", ttl: 5000}
      , (response) ->
        APIServce.treatError(response)

    $scope.unsubscribe = () ->
      Subscription.unsubscribe $scope.channel.subscription.id, ->
        $state.go "feed"

  .controller 'VideosForLaterCtrl', ($scope, SelectedFilters, VideoService ) ->
    $scope.VideoService = VideoService
    $scope.VideoService.clear()
    $scope.VideoService.set("for_later", true)
    $scope.header = "Videos for later"
    $scope.noMoreMessage = "There aren't any more videos for later."
    $scope.emptyMessage = "You haven't set any videos as for later."

  .controller 'WatchedVideosCtrl', ($scope, SelectedFilters, VideoService ) ->
    $scope.VideoService = VideoService
    $scope.VideoService.clear()
    $scope.VideoService.set("watched", true)
    $scope.header = "Watched videos"
    $scope.noMoreMessage = "There aren't any more videos you watched."
    $scope.emptyMessage = "You haven't set any videos as watched."

  .controller 'IgnoredVideosCtrl', ($scope, SelectedFilters, VideoService ) ->
    $scope.VideoService = VideoService
    $scope.VideoService.clear()
    $scope.VideoService.set("ignored", true)
    $scope.header = "Ignored videos"
    $scope.noMoreMessage = "There aren't any more videos you ignored"
    $scope.emptyMessage = "You haven't set any videos as ignored."

  .controller 'SearchInVideosCtrl', ($scope, SelectedFilters, VideoService ) ->
    $scope.VideoService = VideoService
    $scope.VideoService.clear()
    $scope.VideoService.set("query", SelectedFilters.searchQuery)
    $scope.emptyMessage = "There are no videos matching this search."
    $scope.noMoreMessage = "There aren't any more videos matching this search."

  .controller 'VideosAtDateCtrl', ($scope, SelectedFilters, VideoService, $stateParams) ->
    $scope.VideoService = VideoService
    $scope.VideoService.clear()
    $scope.VideoService.set("date", $stateParams.year+"-"+$stateParams.month+"-"+$stateParams.day)
    $scope.header = "Videos from " + moment($stateParams.year+"-"+$stateParams.month+"-"+$stateParams.day).format('D. M. YYYY')
    $scope.noMoreMessage = "No more videos were published on this date"
    $scope.emptyMessage = "We don't have any videos for this date."

  .controller 'FavoritesVideosCtrl', ($scope, SelectedFilters, VideoService ) ->
    $scope.VideoService = VideoService
    $scope.VideoService.clear()
    $scope.VideoService.set("favorites", true)
    $scope.header = "Videos of favorite channels"
    $scope.noMoreMessage = "There aren't any more videos matching this filter setup."
    $scope.emptyMessage = "You haven't got any favorite channels."

  .controller 'OnlyLatestVideoCtrl', ($scope, SelectedFilters, VideoService ) ->
    $scope.VideoService = VideoService
    $scope.VideoService.clear()
    $scope.VideoService.set("only_latest_video", true)
    $scope.header = "Only the latest video from a channel"
    $scope.noMoreMessage = "There aren't any more videos matching this filter setup."
    $scope.emptyMessage = "There aren't any more videos matching this filter setup."
