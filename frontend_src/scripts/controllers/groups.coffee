'use strict'

angular.module('bettersubboxApp.controllers')
  .controller 'GroupListCtrl', ($scope, Group, growl, APIService ) ->
    # managing groups

    $scope.loadGroups = () -> # so groups are reloadeable easily
      Group.get {
        order_by: "name",
        limit: 0,
      }, (data) ->
        $scope.groups = data.objects
      , (response) ->
        APIService.treatError(response)

    $scope.loadGroups() # initial load of groups

    $scope.delete = (group) -> # delete group
      Group.remove {groupId: group.id}, () ->
        growl.success "Group was deleted.", {title: "Success", ttl: 5000}
        $scope.$emit "groups-changed"
        $scope.loadGroups()
      , (response) -> # error
        APIService.treatError(response)
        $scope.loadGroups()


  .controller 'NewGroupCtrl', ($scope, Group, growl, User, $state, APIService ) ->
    # creates new group

    $scope.group = new Group()
    $scope.group.exclude_from_feed = false

    User.get {}, (data) ->
      $scope.group.user = data.objects[0].id # sets the current user
    , (response) ->
      APIService.treatError(response)

    # creates the group
    $scope.save = () ->
      Group.save $scope.group, (group) ->
        growl.success "Group was created.", {title: "Success", ttl: 5000}
        $scope.$emit "groups-changed"
        $state.go "groups.edit", {slug: group.slug}
      , (response) -> # error
        APIService.treatError(response)

  .controller 'EditGroupCtrl', ($scope, Group, growl, User, GroupMembership, Channel, $state,
                                $stateParams, APIService ) ->

    # edit group
    id = $stateParams.slug.split("-")[0]
    $scope.group = Group.get({groupId: id}, (data) ->
      null
    , (response) ->
      APIService.treatError response
    )
    $scope.channelQuery = ""
    $scope.alledChannels = []
    $scope.members = []
    $scope.activeTab = 1

    $scope.loadMembers = () -> # function that realods members of the group
      $scope.group.$promise.then ->
        GroupMembership.query {
          group__id: $scope.group.id
        }, (data) ->
          $scope.members = data.objects
        , (response) ->
          APIService.treatError(response)

    $scope.loadMembers() # initial load

    $scope.save = () -> # change of name
      Group.update $scope.group, (group) ->
        growl.success "Group was edited.", {title: "Success", ttl: 5000}
        $scope.$emit "groups-changed"
      , (response) -> # error
        APIService.treatError(response)

    $scope.filterChannels = (channelQuery) -> # gets channels that matcha a query
      $scope.channelQuery = channelQuery
      if channelQuery
        Channel.query {
          title__icontains: channelQuery
          not_in_group: $scope.group.id
        }, (data) ->
          $scope.filteredChannels = data.objects
        , (response) ->
          APIService.treatError(response)
      else
        $scope.filteredChannels = []

    $scope.filterAllChannels = () ->
      return $scope.alledChannels.filter((channel) ->
        $scope.members.filter((mem) -> mem.channel.id == channel.id).length == 0
      )

    $scope.allChannels = () -> # loads all channels
      Channel.query {
        limit: 0
        order_by: "title"
      }, (data) ->
        $scope.alledChannels = data.objects
      , (response) ->
        APIService.treatError(response)

    $scope.activateAllTab = () -> # loading of all channels takes a while, so they are loaded on opening of the second tab
      $scope.activeTab = 2
      if $scope.alledChannels.length == 0
        $scope.allChannels()

    $scope.add = (channel) -> # adds membership
      membership = new GroupMembership()
      membership.group = $scope.group.id
      membership.channel = channel.id
      GroupMembership.save membership, (data) ->
        growl.success "Added channel to group.", {title: "Success", ttl: 5000}
        $scope.members.push data
        $scope.filterChannels($scope.channelQuery) # realods the query so the added channel is not there
      , (response) -> #error
        APIService.treatError(response)

    $scope.remove = (membership) -> # removes from group
      GroupMembership.remove {groupMembershipId: membership.id}, () ->
        growl.success "Removed channel from group.", {title: "Success", ttl: 5000}
        $scope.members = $scope.members.filter (member) -> member.id != membership.id
        $scope.filterChannels($scope.channelQuery) # realods the query so the added channel can be there
      , (response) -> # error
        APIService.treatError(response)


