'use strict'

angular.module('bettersubboxApp.controllers')
  .controller 'ChannelsListCtrl', ($scope, Channel, Subscription, growl, APIService ) ->
    $scope.offset = 0
    $scope.limit = 20
    $scope.filter_query = ""
    $scope.channels = []
    $scope.total_count = 0
    $scope.order = "channel__title"

    $scope.goTo = (offset) ->
      $scope.offset = offset

    $scope.$watch "offset", ->
      $scope.load()

    $scope.search = ->
      $scope.offset = 0
      $scope.load()

    $scope.change_order = (order) ->
      $scope.order = order
      $scope.offset = 0
      $scope.load()

    $scope.order_active = (order) ->
      return $scope.order == order or $scope.order == "-" + order

    $scope.other_order = (order) ->
      if order[0] == "-"
        return order.replace("-", "")
      else
        return "-" + order

    $scope.generate_order = (order) ->
      if $scope.order_active(order)
        return $scope.other_order($scope.order)
      return order

    $scope.load = () ->
      Subscription.query {
        limit: $scope.limit,
        offset: $scope.offset,
        title_icontains: $scope.filter_query
        order_by: $scope.order,
        make_channel_full: true,
        calculate_percentage: true,
        include_last_video_published: true
      }, (data) ->
        $scope.total_count = data.meta.total_count
        $scope.subscriptions = data.objects
      , (response) ->
        APIService.treatError(response)

    $scope.channelThumbnail = (subscription) ->
      return subscription.channel.thumbnail

    $scope.favorite_change = (subscription) ->
      subscription.favorite = !subscription.favorite
      tmp = angular.copy(subscription)
      tmp.channel = tmp.channel.id
      Subscription.update tmp, () ->
        if subscription.favorite
          growl.success "Channel favorited", {title: "Success", ttl: 5000}
        else
          growl.success "Channel unfavorited", {title: "Success", ttl: 5000}
      , (response) ->
        APIService.treatError(response)

    $scope.unsubscribe = (subscription) ->
      Subscription.unsubscribe subscription.id, ->
        $scope.load()
