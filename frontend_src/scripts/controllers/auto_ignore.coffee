'use strict'

angular.module('bettersubboxApp.controllers')
  .controller 'ManageAutoIgnoresCtrl', ($scope, AutoIgnore, growl, Channel, APIService ) ->
    # managing ignores

    $scope.loadIgnores = () -> # so ignores are reloadeable easily
      AutoIgnore.get {limit: 1000}, (data) ->
        $scope.ignores = data.objects
      , (response) ->
        APIService.treatError(response)

    $scope.loadIgnores() # initial load of ignores

    $scope.delete = (autoIgnore) -> # delete autoIgnore
      AutoIgnore.remove {autoIgnoreId: autoIgnore.id}, () ->
        growl.success "Auto Ignore was deleted.", {title: "Success", ttl: 5000}
        $scope.loadIgnores()
      , (response) -> # error
        APIService.treatError(response)

    $scope.ignore = new AutoIgnore()

    $scope.$watch "channelQuery", -> # load the channels matching the search query
      Channel.query {
        title__icontains: $scope.channelQuery,
        limit: 5
      }, (data) ->
        $scope.filtered_channels = data.objects
      , (response) ->
        APIService.treatError(response)

    $scope.save = ->

      # check the required fields
      if not $scope.ignore.channel
        growl.error "You have to select a channel", {title: "Error", ttl: 10000}
        return
      else if not $scope.ignore.string
        growl.error "You have to input a string", {title: "Error", ttl: 10000}
        return

      ignore_copy = angular.copy($scope.ignore)  # create a copy so when the channel is updated to be only the id the view isn't mesed up
      ignore_copy.channel = ignore_copy.channel.id

      AutoIgnore.save ignore_copy, (data) ->
        $scope.loadIgnores()
        $scope.ignore = new AutoIgnore() # empty the form
        growl.success "You created the ignore.", {title: "Success", ttl: 5000}
      , (response) ->
        APIService.treatError(response)

