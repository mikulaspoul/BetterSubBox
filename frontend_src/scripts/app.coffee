angular.module('infinite-scroll').value('THROTTLE_MILLISECONDS', 500)


angular.module 'bettersubboxApp.controllers', []
angular.module 'bettersubboxApp.filters', []
angular.module 'bettersubboxApp.directives', []

angular
  .module('ErrorCatcher', [])
    .factory '$exceptionHandler', ->
      return (exception, cause) ->
        console.error(exception.stack)
        event_id = Raven.lastEventId()
        Raven.captureException(exception)
        if event_id != Raven.lastEventId()
          Raven.showReportDialog();


"""
Base set up of the angular application, router and so.
"""

angular
  .module('bettersubboxApp', [ # dependencies
    'ngRaven',
    'ErrorCatcher',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'bettersubboxApp.controllers',
    'bettersubboxApp.filters',
    'bettersubboxApp.directives',
    'bettersubboxApp.APIService',
    'bettersubboxApp.VideoService',
    'bettersubboxApp.CacheService',
    'bettersubboxApp.SearchService',

    'ui.router',
    'ui.bootstrap',
    'ui.bootstrap.datetimepicker',
    'angular-loading-bar',
    'mikicz.bootstrap-switch',
    'angular-growl',
    'youtube-embed',
    'LocalStorageModule',
    'infinite-scroll'
  ])
  .config ($stateProvider, $urlRouterProvider, $httpProvider, $provide, localStorageServiceProvider, growlProvider) ->
    $urlRouterProvider.when('', '/')
    $urlRouterProvider.otherwise "/404"
    $stateProvider
      .state 'home',
        url: '/'
        controller: "HomeCtrl"

      .state 'feed',
        url: '/feed'
        templateUrl: '/static/views/videos.html'
        controller: 'FeedVideoCtrl'

      .state 'videos',
        abstract: true
        url: '/videos'
        template: '<ui-view/>'

      .state 'videos.group',
        url: '/group/{slug:[a-zA-Z0-9-_]+}'
        controller: 'GroupsVideosCtrl'
        templateUrl: '/static/views/videos.html'

      .state 'videos.channel',
        url: '/channel/{slug:[a-zA-Z0-9-_]+}'
        controller: 'ChannelsVideosCtrl'
        templateUrl: '/static/views/videos.html'

      .state 'videos.for_later',
        url: '/for_later'
        templateUrl: '/static/views/videos.html'
        controller: 'VideosForLaterCtrl'

      .state 'videos.watched',
        url: '/watched'
        templateUrl: '/static/views/videos.html'
        controller: 'WatchedVideosCtrl'

      .state 'videos.ignored',
        url: '/ignored'
        templateUrl: '/static/views/videos.html'
        controller: 'IgnoredVideosCtrl'

      .state 'videos.search',
        url: '/search'
        templateUrl: '/static/views/videos.html'
        controller: 'SearchInVideosCtrl'

      .state 'videos.date',
        url: '/date/{year:[0-9]*}/{month:[0-9]*}/{day:[0-9]*}'
        templateUrl: '/static/views/videos.html'
        controller: 'VideosAtDateCtrl'

      .state 'videos.favorites',
        url: '/favorites'
        templateUrl: '/static/views/videos.html'
        controller: 'FavoritesVideosCtrl'

      .state 'videos.only_latest_video',
        url: '/only_latest_video'
        templateUrl: '/static/views/videos.html'
        controller: 'OnlyLatestVideoCtrl'

      .state 'settings',
        url: '/settings'
        templateUrl: '/static/views/settings.html'
        controller: 'SettingsCtrl'

      .state 'auto_ignores',
        url: '/settings/auto_ignores'
        templateUrl: '/static/views/auto_ignores.html'
        controller: 'ManageAutoIgnoresCtrl'

      .state '404',
        url: '/404',
        templateUrl: '/static/views/error_pages/404.html'
        controller: "ErrorCtrl"

      .state '401',
        url: '/401',
        templateUrl: '/static/views/error_pages/401.html'
        controller: "ErrorCtrl"

      .state '500',
        url: '/500',
        templateUrl: '/static/views/error_pages/500.html'
        controller: "ErrorCtrl"

      .state 'statistics',
        url: '/statistics',
        templateUrl: '/static/views/statistics.html'
        controller: 'StatisticsCtrl'

      .state 'channels',
        abstract: true
        url: '/channels'
        template: '<ui-view/>'

      .state 'channels.list',
        url: '/'
        controller: 'ChannelsListCtrl'
        templateUrl: '/static/views/channels/list.html'

      .state 'groups',
        abstract: true
        url: '/settings/groups'
        template: '<ui-view/>'

      .state 'groups.list',
        url: '/'
        controller: 'GroupListCtrl'
        templateUrl: '/static/views/groups/list.html'

      .state 'groups.new',
        url: '/new'
        controller: 'NewGroupCtrl'
        templateUrl: '/static/views/groups/new.html'

      .state 'groups.edit',
        url: '/{slug:[a-zA-Z0-9-_]+}/edit'
        controller: 'EditGroupCtrl'
        templateUrl: '/static/views/groups/edit.html'

    # csrf protection
    $httpProvider.defaults.xsrfCookieName = 'csrftoken'
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken'
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
    $httpProvider.defaults.headers.common['X-CSRFToken'] = $('input[name=csrfmiddlewaretoken]').val()

    localStorageServiceProvider.setPrefix('bettersubbox')
    growlProvider.globalDisableCountDown(true)

  .run ($rootScope, $state, User, VideoService, APIService, $timeout, growl ) ->

    # changes state
    initialTransition = true
    $rootScope.$on '$stateChangeStart', (event, next, nextParams) ->
      if not initialTransition
        return
      VideoService.disable_infinite = true
      event.preventDefault()
      initialTransition = false
      $state.transitionTo(next, nextParams)
      initialTransition = true

    $rootScope.$on "$stateChangeSuccess", ->
      $timeout(->
        VideoService.disable_infinite = false
      , 150)

    $rootScope.$on "$stateChangeSuccess", (event, next, nextParams) ->
      if $state.current.name == 'videos.for_later' or $state.current.name == 'videos.watched' or $state.current.name == 'videos.ignored'
        VideoService.displayIncludeFilters(false)
      else
        VideoService.displayIncludeFilters(true)

    $rootScope.user = window.user

    if $rootScope.user.allow_elevator_sounds
      elevator_settings = {
        mainAudio: "/static/audio/elevator.mp3",
        endAudio: "/static/audio/ding.mp3",
      }
    else
      elevator_settings = {}

    elevator_settings["duration"] = 1000

    $rootScope.elevator = new Elevator elevator_settings

    $rootScope.newest = () -> # if the state is on newest already, reaload the videos...
      if $state.current.name == "feed" # you cant go to a state you are already on, so just reloads the parameter -> VideoService applies the change
        VideoService.set("newest", "newest")
      else # goes to the search, query is stored in SelectedFilters
        $state.go("feed")

    $rootScope.$on "groups-changed", ->
      $rootScope.$broadcast "groups-changed-2"

    $rootScope.$on "layout-change", ->
      if VideoService.layout != $rootScope.user.layout
        $rootScope.user.layout = VideoService.layout
        User.update $rootScope.user, (user) ->
          $rootScope.user = user
          VideoService.user = user

        , (response) ->
          APIService.treatError response

    $rootScope.$on "user-updated",  ->
      $rootScope.user = VideoService.user

    for message in window.serialized_messages
      growl[message["title"]](message["text"], {
        "title": message["title"].charAt(0).toUpperCase() + message["title"].slice(1),
        ttl: 10000
      })
