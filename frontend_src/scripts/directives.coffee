'use strict'

angular.module('bettersubboxApp.directives')

  .directive 'mAppLoading', ($animate) ->
    # http://www.bennadel.com/blog/2758-creating-a-pre-bootstrap-loading-screen-in-angularjs.htm


    AfterLoad = (scope, element, attributes) ->
      $gif = $(element).find(".loading-gif")
      $logo = $(element).find(".logo")
      $originalLogo = $(".navbar-brand").find("img").first()
      actualAbsolutePosition = $logo.offset()
      originalLogoOffset = $originalLogo.offset()
      originalLogoWidth = $originalLogo.width()
      originalLogoHeight = $originalLogo.height()

      $gif.fadeOut(1000)
      $logo.css(
        position: 'fixed'
        top: actualAbsolutePosition.top
        left: actualAbsolutePosition.left
      ).animate {
        left: originalLogoOffset.left
        top: originalLogoOffset.top
        width: originalLogoWidth
        height: originalLogoHeight
      }, 1000, ->

        $(element).fadeOut(1000, ->
          element.remove()
          scope = element = attrs = null
        )


      return

    link = (scope, element, attributes) ->
      AfterLoad(scope, element, attributes)

    {
      link: link
      restrict: 'C'
    }
