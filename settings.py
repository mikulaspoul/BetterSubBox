# -*- coding: utf-8 -*-
"""
Django settings for bettersubbox project.
"""

import sys
from pathlib import Path
import copy
import six
from django.utils.log import DEFAULT_LOGGING
from django.core.exceptions import ImproperlyConfigured

# PATH, which modules are loaded....
CWD = Path(__file__).resolve().parent

sys.path.insert(0, CWD)
sys.path.insert(0, str(CWD / "apps"))


DEBUG = True
ALLOWED_HOSTS = ["*"]
FAKE_AUTH = False
OFFLINE_MODE = False
REDIS_DB = 0
REDIS_PASSWORD = None

LOG_DEBUG = DEBUG

ADMINS = (
    ('Mikuláš Poul', 'mikulaspoul@gmail.com'),
)

NO_REPLY_EMAIL = "no-reply@bettersubbox.com"
INFO_MAIL = "info@bettersubbox.com"

# urls config
ROOT_URLCONF = 'urls'

WSGI_APPLICATION = 'wsgi.local.application'

USE_I18N = True

USE_L10N = True

USE_TZ = True

TIME_ZONE = 'Europe/Prague'

LANGUAGE_CODE = 'en'

MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'apps.core.middleware.FakeAuth' if FAKE_AUTH else 'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'apps.core.middleware.ConnectionErrorMiddleware'
]

AUTHENTICATION_BACKENDS = (
    'apps.core.backends.CustomGoogleOAuth2',
    'django.contrib.auth.backends.ModelBackend',
)


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            str(CWD / "templates")
        ],
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.request',
                'apps.core.context_processors.debug',
                'apps.core.context_processors.write_access_cookie',
                'apps.core.context_processors.serialize_messages',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
            ],
            'loaders': [
                ('django.template.loaders.cached.Loader', [
                    'django.template.loaders.filesystem.Loader',
                    'django.template.loaders.app_directories.Loader',
                ]),
            ],
        },
    },
]

# only testing
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


INSTALLED_APPS = (
    "redis_cache",
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'social_django',
    'raven.contrib.django.raven_compat',
    'django_cron',
    'django_extensions',
    'jsoneditor',

    'apps.channel',
    'apps.group',
    'apps.video',
    'apps.custom_user',
    'apps.news',
    'apps.core',
    'apps.youtube'
)


# Static files, only on devel
STATIC_URL = '/static/'
STATIC_ROOT = (CWD / 'collected_static').resolve()
STATICFILES_DIRS = ((CWD / 'static').resolve(), )


# custom user model
AUTH_USER_MODEL = 'custom_user.CustomUser'
SOCIAL_AUTH_USER_MODEL = 'custom_user.CustomUser'

# OAUTH2 settings
SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL = True
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = [
    'https://www.googleapis.com/auth/youtube.readonly'
]

YOUTUBE_WRITE_SCOPE = "https://www.googleapis.com/auth/youtube"

# LOGIN
LOGIN_URL = "/login/"

LOGIN_REDIRECT_URL = '/'

# Tastypie
TASTYPIE_DEFAULT_FORMATS = ['json']
APPEND_SLASH = True
TASTYPIE_ALLOW_MISSING_SLASH = True

CACHE_TIME = 24 * 60 * 60
DISCONNECTED_KEEP_TIME = 30 * 24 * 60 * 60  # in seconds
INACTIVE_KEEP_TIME = 365 / 2 * 24 * 60 * 60  # in seconds
COUNT_CACHE_TIME = 1 * 60 * 60
NUMBER_OF_PRIORITIES = 6
SYNCLOG_KEEP_TIME = 7  # in days
USE_SYNC_LOGS = False
USE_COUNTER = False

REDIS_HOST = 'localhost'
REDIS_PORT = 6379

CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.social_auth.associate_by_email',  # <--- enable this one
    'social_core.pipeline.user.create_user',
    'apps.core.pipeline.disable_multiple_accounts',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
    'apps.core.pipeline.undelete',
    'apps.core.pipeline.log'
)

SOCIAL_AUTH_DISCONNECT_PIPELINE = (
    'social_core.pipeline.disconnect.get_entries',
    'social_core.pipeline.disconnect.revoke_tokens',
    'social_core.pipeline.disconnect.disconnect'
)

DISCONNECT_REDIRECT_URL = "/user/disconnect_callback/"

CRON_CLASSES = [
    'apps.youtube.cron.GetNewVideosJob',
    'apps.custom_user.cron.DeleteDisconnectedJob',
    'apps.custom_user.cron.DeleteInactiveJob',
    'apps.youtube.cron.UpdateChannelsJob',
    'apps.youtube.cron.DeleteUnusedChannelsJob',
    'apps.channel.cron.UpdateChannelsCountsAndPrioritiesJob',
    'apps.youtube.cron.DeleteOldSynLogsJob',
]

SHELL_PLUS = "ipython"


try:
    from database_settings import *  # noqa
except ImportError:
    try:
        from local_settings import *  # noqa
    except ImportError:
        pass

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'filters': {
        "required_debug_false": {
            "()": "django.utils.log.RequireDebugFalse"
        }
    },
    'root': {
        'handlers': ['sentry'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(asctime)s %(message)s'
        },
    },
    'handlers': {
        'sentry': {
            'level': 'DEBUG' if LOG_DEBUG else 'INFO',
            'filters': ["required_debug_false"],
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler'
        },
        'console': {
            'level': 'DEBUG' if LOG_DEBUG else 'INFO',
            'class': 'logging.StreamHandler',
        }
    },
    'loggers': {
        'db': {
            'handlers': ['console', 'sentry'],
            'level': 'DEBUG' if LOG_DEBUG else 'INFO'
        },
        'django.request': {
            'handlers': ['console', 'sentry'],
            'level': 'WARNING',
            'propagate': True,
        },
        'django.request.tastypie': {
            'handlers': ['console', 'sentry'],
            'level': 'WARNING',
            'propagate': True,
        },
        'celery': {
            'handlers': ["sentry"],
            'level': 'WARNING' if not DEBUG else "INFO",
            'propagate': True
        },
        'keys': {
            'handlers': ['console', 'sentry'],
            'level': 'WARNING',
            'propagate': False
        },
        'raven': {
            'level': 'INFO',
            'handlers': ['console'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'INFO',
            'handlers': ['console'],
            'propagate': False,
        },
        # 'django.db.backends': {
        #     'level': 'DEBUG',
        #     'handlers': ['console'],
        # }
    }
}
try:
    SECRET_KEY  # noqa: F405
except NameError:
    raise ImproperlyConfigured("You must specify SECRET_KEY")
try:
    RAVEN_CONFIG  # noqa: F405
except NameError:
    raise ImproperlyConfigured("You must specify RAVEN_CONFIG")
try:
    DATABASES  # noqa: F405
except NameError:
    raise ImproperlyConfigured("You must specify DATABASES")
try:
    SOCIAL_AUTH_GOOGLE_OAUTH2_KEY  # noqa: F405
except NameError:
    raise ImproperlyConfigured("You must specify SOCIAL_AUTH_GOOGLE_OAUTH2_KEY")
try:
    SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET  # noqa: F405
except NameError:
    raise ImproperlyConfigured("You must specify SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET")
try:
    YOUTUBE_API_KEY  # noqa: F405
except NameError:
    raise ImproperlyConfigured("You must specify YOUTUBE_API_KEY")
try:
    SENTRY_PUBLIC_DSN  # noqa: F405
except NameError:
    raise ImproperlyConfigured("You must specify SENTRY_PUBLIC_DSN")

broker_password = ""
if not DEBUG:
    CACHES = {
        'default': {
            'BACKEND': 'redis_cache.RedisCache',
            'LOCATION': REDIS_HOST + ":" + str(REDIS_PORT),
            "OPTIONS": {
                "DB": REDIS_DB,
            }
        },
    }

else:
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        }
    }

if REDIS_PASSWORD:
    if CACHES['default']['BACKEND'] == 'redis_cache.RedisCache':
        CACHES["default"]["OPTIONS"]["PASSWORD"] = REDIS_PASSWORD
    broker_password = ":{}@".format(REDIS_PASSWORD)


BROKER_URL = 'redis://{password}{host}:{port}/{db}'.format(host=REDIS_HOST,
                                                           port=REDIS_PORT,
                                                           db=REDIS_DB,
                                                           password=broker_password)

CELERY_RESULT_BACKEND = BROKER_URL
